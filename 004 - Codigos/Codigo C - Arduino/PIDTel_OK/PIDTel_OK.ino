
#define A1 5
#define B1 4
#define A2 7
#define B2 6
#define M1_1 8
#define M2_1 9
#define M1_2 12
#define M2_2 13

#define PWM1 3
#define PWM2 11

#define prescaler 256
#define CLKfreq 16000000


#define PWMMAN1_ALT  72
#define PWMMAN2_ALT  72
#define PWMMAX_ALT   90
#define PWMMIN_ALT   72
      


#define PWMMAN_AZI  67
#define PWMMAX_AZI  85
#define PWMMIN_AZI  67


#define MANU  'M' 
#define AUTO  'A' 




#define KP1 0.2
#define KI1 0
#define KD1 0
#define KP2 0.2
#define KI2 0
#define KD2 0

#define Imax 15
#define Imin -15

String inputString = "";         // a String to hold incoming data
boolean stringComplete = false;  // whether the string is complete

int AA = 0;
String Cadena = "", Cad_ant = "";
int Tmuestra;
char MODO = MANU;
int Espia = 0;
//int MAT[4][4] = {{0, 1 , -1, 'E'}, { -1 , 0, 'E', 1}, {1, 'E', 0 , -1 }, {'E', -1 , 1, 0}};
int MAT[4][4] = {{0, 1 , -1, 0}, { -1 , 0, 0, 1}, {1, 0, 0 , -1 }, {0, -1 , 1, 0}};
String Message;
String flag = "P";
String comma = ",";
int P_ALT = 0, P_AZI = 0, SP_ALT = 0, SP_AZI = 0;

int Sentido_ALT = 0, Sentido_AZI = 0;

class MotorEncoder {
  private:

    int  pwm;    //Valor PWM
    int  Sentido;//Sentido de giro en funcionde PWM
    int  Sentido_ant;// Comprueba el cambio de sentido
    byte PWMpin; // PIN PWM
    byte M1;     // Pin motor 1
    byte M2;     // Pin motor 1

    byte AB;     // Valor GRAY encoder actual
    byte AB_ant; // Valor GRAY anterior
    byte pinA;   // Pin A Enc
    byte pinB;   // Pin B Enc

    double kp = 0.2; //KP1; // Constante kp
    double ki = 2; //KI1*Tmuestra; // Constante ki para Tmuestra
    double kv = 0.005; //KD1/Tmuestra; // Constante kv para Tmuestra
    float I = 0;                    // Valor elemento integral
    int pid = 0;

    int Ptot = 0;
    int P = 0;                      // Valor de los pasos
    int error = 0;                  // Error actual
    int error_ant = 0;              // Error anterior
    int SPoint =    0;                 // SetPoint actual
    int SPoint_ant = 0;             // SetPoint actual
    int Sentido_m = 0;

    int  Pwm_min = 0;
    int  Pwm_max = 0;
    int  Pwm_man = 0;

  public:
    MotorEncoder (byte encA , byte encB, byte M_1, byte M_2, byte PWM_pin) { // Declaracion inicial del objeto y asignacion  de los pines a Variable
      pinA = encA;
      pinB = encB;
      M1 = M_1 - 8;
      M2 = M_2 - 8;
      PWMpin = PWM_pin;
      pwm = 0;
      AB  = 0;
      AB_ant  = 0;

    }
    void   PIDpwm() {                                                    // Funcion PID
      //  char Tmuestra;
      error = SPoint - P;
      //  double error_ant = SPoint_ant-P_ant

      I += error * Tmuestra;
      if (I < Imin) {
        I = Imin;
      }
      if (I > Imax) {
        I = Imax;
      }
      int dInput = SPoint_ant - SPoint;
      int de = error - error_ant;
      int dedt = de;
      double Output = kp * error + ki * I + kv * dedt;
      SPoint_ant = SPoint;
      //  P_ant=P;
      error_ant = error;
      pid = constrain(Output, -Pwm_max, Pwm_max);
    }

    
    void SetPWMSettings(int MIN,int MAX,int MAN){
      Pwm_min = MIN;
      Pwm_max = MAX;
      Pwm_man = MAN;
    }

    void SetPIDSettings(float Kp,float Kv,float Ki){
      kp=Kp;
      kv=Kv;
      ki=Ki;
    }


    void  Setup() { 
      
      
      
      // Setup pin PWM
      DDRD &= ~1 << pinA;
      DDRD &= ~1 << pinB;
      PORTD |= 1 << pinA;
      PORTD |= 1 << pinB;
     
      
      if (PWMpin == PWM1) {
        DDRD  |=  1 << 3;
        PORTD |=  1 << 3;
     }
      if (PWMpin == PWM2) {
       DDRB  |=  1 << 3;
       PORTB |=  1 << 3;
      }
          
      DDRB |= 1 << M1;
      DDRB |= 1 << M2;

      
      //    pinMode(M1,OUTPUT);
      //    pinMode(M2,OUTPUT);
//      analogWrite(PWMpin, 0);
    }
    
    int   MANpwm() {                                                    // Funcion MANUAL según sentido pulsado
      if (PWMpin == PWM1) {
        Sentido_m = Sentido_ALT;
      }
      if (PWMpin == PWM2) {
        Sentido_m = Sentido_AZI;
      }
      return Pwm_man * Sentido_m;
    }

    void  PWMDirection() {                                              // Sentido pin según pwm ->> Si >0 un sentido Si<0 el contrario
//      Sentido=pwm/abs(pwm);
//      if (Sentido != Sentido_ant){
      if (pwm > 0) {
            PORTB |=  (1<<M1);
            PORTB &= ~(1<<M2);
//        digitalWrite(M1 + 8, HIGH);
//        digitalWrite(M2 + 8, LOW);
      }
      if (pwm == 0) {
           PORTB &= ~(1<<M1);
           PORTB &= ~(1<<M2);
//        digitalWrite(M2 + 8, LOW);
//        digitalWrite(M1 + 8, LOW);
      }
      if (pwm < 0) {
            PORTB &= ~(1<<M1);
            PORTB |=  (1<<M2);
//        digitalWrite(M1 + 8, LOW);
//        digitalWrite(M2 + 8, HIGH);
      }}
      
    void  Count1channel() {
      AB_ant = AB;

      AB = (PIND & B11 << pinB) >> pinB;
      int A = AB >> 1;
      int B = AB & 1;
      int A_ant = AB_ant >> 1;
      int B_ant = AB_ant & 1;
      int inc;
      //  PORTD|=1<<7;
      if (B_ant != B) {

        if (pwm >= 0) {
          inc = 1 ;
        }
        else {
          inc = -1;
        }
      }
      else {
        inc = 0;
      }
      P += inc;

    }
    
    void  Count() {                                                     // Lectura de Pines y Cuenta GRAY
      AB_ant = AB;

      AB = (PIND & B11 << pinB) >> pinB;

      //  PORTD|=1<<7;

      int inc = MAT[AB_ant][AB];

      P += inc;
      // if (P>=3200){P-=3200;}
      // if (P<=-3200){P+=3200;}
 

    }
    
    int   GetP() {
      return P;
      
//return pwm;
    }


    void  SetSP(int SPoint_com) {
      SPoint = SPoint_com;
    }

    void  Modos() {

      if  (MODO == AUTO) {

        pwm = pid; //Asigna a la variable el valor del sistema PID

      }
      if (MODO == MANU) {

        pwm = MANpwm();
      }


    int abspwm = abs(pwm);
   


        if (pwm < 5 and pwm > 5){
            pwm = 0;
        }
                       
        if (pwm > 5 and pwm < Pwm_min){
            pwm = Pwm_min;
        }

       if (pwm < -5 and pwm > -Pwm_min){
            pwm = -Pwm_min;
            } 
            
       abspwm=abs(pwm);

//      if (PWMpin == PWM1) {
//
//        OCR2B = abspwm;
//
//         
//      }
//      //        OCR2B=constrain(0,255,abs(pwm));}
//      if (PWMpin == PWM2) {
//
//        OCR2A = abspwm;
//      }
      //        OCR2A=constrain(0,255,abs(pwm));}

      analogWrite(PWMpin,abspwm);
    }

};

MotorEncoder MALTURA(A1, B1, M1_1, M2_1, PWM1);
MotorEncoder MAZIMUT(A2, B2, M1_2, M2_2, PWM2);

void setup (void) {
  Serial.begin (115200);   // debugging
 
  DDRD |= B11111100; // 0,1 tx rx y 4,5,6,7 input encoders 3 pwm timer2B(OCR2B)
  // 1s Son OUTPUT 0s Son INPUT
  // TODOS OUTPUT en principio salvo el Rx y Tx
  DDRB |= B111111;   // 11 pwm timer2A OCR2A y 12/13 8/9 pines motor
  // TODOS OUTPUT en principio salvo el Rx y Tx

  Tmuestra = (OCR1A * prescaler + 1) / CLKfreq;
  
  MALTURA.Setup();
  MAZIMUT.Setup();

  MALTURA.SetPWMSettings(PWMMIN_ALT,PWMMAX_ALT,PWMMAN1_ALT);
  MAZIMUT.SetPWMSettings(PWMMIN_AZI,PWMMAX_AZI,PWMMAN_AZI);
  
  MALTURA.SetPIDSettings(KP1,KD1,KI1);
  MAZIMUT.SetPIDSettings(KP2,KD2,KI2);
       
  //  OCR2A = val ;  // analogWrite (11, val)
  //  OCR2B = val ;  // analogWrite (3, val)


  inputString.reserve(1);


  Ftimer();

  FastPWMtimer2();




}  // end of setup

void loop (void) {


  if (stringComplete) {

    Cadena = inputString;
    Substract_msg();
    Send_msg();
    stringComplete = false;
    inputString = "";
  }

  MALTURA.PWMDirection();
  MAZIMUT.PWMDirection();

  
  MALTURA.Modos();
  MAZIMUT.Modos();
  
  MALTURA.Count();
  MAZIMUT.Count();

  P_ALT = MALTURA.GetP();
  P_AZI = MAZIMUT.GetP();
  
  MALTURA.SetSP(SP_ALT);
  MAZIMUT.SetSP(SP_AZI);
 
 //MALTURA.Count1channel();
}













