ISR(TIMER1_COMPA_vect){
MALTURA.PIDpwm();
MAZIMUT.PIDpwm(); 

  }
 void Ftimer(void){
    cli();//stop interrupts
//set timer1 interrupt at 10Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = 6249;// = (16*10^6) / (10*256) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 and CS10 bits for 256 prescaler
  TCCR1B |= (1 << CS12);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);
  sei();//allow interrupts
  }

 void FastPWMtimer2(void){
  

  TCCR2A = _BV(COM2A1) | _BV(COM2B1) | _BV(WGM21) | _BV(WGM20);
  TCCR2B = _BV(CS22);
  }

 void serialEvent(void){
  
//Solo emitimos un mensaje cuando se detecta que es necesario mandarlo a través del puerto serie
  while (Serial.available()){
    char inChar = (char)Serial.read();
    inputString += inChar;
    if (inChar == '\n'){
        stringComplete = true;
    }          
  }
}
 
 void Send_msg(void){
   Message = flag + P_ALT + comma + P_AZI;
//   Message = flag + Sentido_ALT + comma + Sentido_AZI;
   Serial.println(Message);
  }

 void Substract_msg(void){
//  if (not strcmp(Cadena[0],MANU) or not strcmp(Cadena[0],AUTO)){
//    MODO=Cadena[0];
  if (Cadena[0]== MANU or Cadena[0]==AUTO){
    MODO=Cadena[0];
  }
  if (Cadena[0]==AUTO ){
    SP_ALT = Cadena.substring(1,7).toInt();
    SP_AZI = Cadena.substring(7,13).toInt();
  }
    if (Cadena[0]==MANU ){
    Sentido_ALT = Cadena.substring(1,3).toInt();
    Sentido_AZI = Cadena.substring(3,5).toInt();
  }
  }

