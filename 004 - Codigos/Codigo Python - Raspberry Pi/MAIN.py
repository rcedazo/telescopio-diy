# -*- coding: utf-8 -*-
#!/usr/bin/python3

#importación librerías código Dani

#Importamos las librerias necesarias

import re
import json
import os
##import pandas as pd

import RPi.GPIO as GPIO
GPIO.setwarnings(False)
from RPLCD import CharLCD

#Importamos codigos generados por nosotros
import MenuModo
import ModoAutomatico
import ModoManual
import Nivelacion_Montura
import RegistroEntradas
import ControlMando
import pandas as pd

from FuncionesGenerales import LCD_ON, write_to_lcd, loop_string, Button_State, Lectura_Boton, millis, write_LCD

#Importación librerías jose

import time
import threading
import socket
import serial

#import tty, ctermios, sys
import tty, termios, sys

from math import  sin, cos ,tan , asin, acos, atan,pi
from datetime import datetime, timedelta

import COORD
import MOT_ENC
##import MOT_ENC_INTERR as MOT_ENC
from Const import Const as c

#################################################################################################
                

class HW(threading.Thread):  ## POSEE TODO LO QUE TIENE QUE VER CON MOTORES: PASOS, PID ETC
        def __init__(self,PCA1,PCB1,A1,B1,E1,CPR1,GR1,TR_r1,
                     PCA2,PCB2,A2,B2,E2,CPR2,GR2,TR_r2):
                threading.Thread.__init__(self)
                self.MALTURA=MOT_ENC.MOTENC(PCA1,PCB1,A1,B1,E1,CPR1,GR1,TR_r1)
                self.MAZIMUT=MOT_ENC.MOTENC(PCA2,PCB2,A2,B2,E2,CPR2,GR2,TR_r2)

                #Variables de COM_Raspduino
                self.Modo = "M"          
                self.msg = str(self.Modo) +  str(self.MALTURA.MOT.SP) +  str(self.MAZIMUT.MOT.SP) +  "\n"
                self.msgpasos= ""
                self.BotonAL = 0
                self.BotonAZ = 0
                self.SER = 0
                
        def run(self):
##          self.MALTURA.start()
##              self.MAZIMUT.start()
                self.COM_Raspduino()
                pass

        def COM_Raspduino(self):

                cont = 0
                interv=0.1
                Tant=0
                data=0

                #Abrimos el puerto serial con el Arduino

                try:

                        self.SER = serial.Serial('/dev/ttyACM0',115200, timeout=1)
                        time.sleep(1)

                except:

                        try:
                                self.SER = serial.Serial('/dev/ttyACM1',115200, timeout=1)
                                time.sleep(1)
                                
                        except:

                                try:
                                        self.SER = serial.Serial('/dev/ttyACM2',115200, timeout=1)
                                        time.sleep(1)

                                except:

                                        try:
                                                self.SER = serial.Serial('/dev/ttyACM3',115200, timeout=1)
                                                time.sleep(1)

                                        except:

                                                print("NO SE ENCUENTRA EL ARDUINO EN NINGUN PUERTO")
 

                        
                
                #Provocamos un reseteo manual de la placa para leer desde el principio
                self.SER.setDTR(False)
                self.SER.flushInput()
                self.SER.setDTR(True)

                while True:

                #Actualizamos el mensaje a enviar

                        if self.Modo == 'A':
                        
                                self.msg = str(self.Modo) +  str(int(self.MALTURA.MOT.SP)).zfill(6) +  str(int(self.MAZIMUT.MOT.SP)).zfill(6) +  "\n"

                        elif self.Modo == 'M':

                                self.msg = str(self.Modo) +  str(int(self.BotonAL)).zfill(2) +  str(int(self.BotonAZ)).zfill(2) +  "\n"
                                
##                        elif self.Modo=='P':
##
##                                self.msg = str(self.Modo)+ "\n"
                
                #Mandamos un mensaje con toda la información                                    
        
                        Tnow=time.perf_counter()
                                                                
                        if Tnow - Tant >= interv:

##                                print(self.msg)
                                                        
                                self.SER.write(bytes(self.msg,'UTF-8'))

##                              Tant=Tnow

                                try:

                                        #Leemos la información cuando la tenemos en el buffer de espera

                                        if self.SER.read(1) == bytes("P", 'UTF-8'):
                                                                
                                                self.msgpasos = self.SER.readline()
                                                self.msgpasos = self.msgpasos.decode('ascii',errors='replace')

                                                P1, P2 = self.msgpasos.split(",")

                                                self.MALTURA.ENC.P = int(P1)
                                                self.MAZIMUT.ENC.P = int(P2)

                                except:
                                        pass

##                                print(str(self.MALTURA.ENC.P) + " " + str(self.MAZIMUT.ENC.P))

                                Tant = Tnow

#CREAMOS UNA CLASE PARA EL COM QUE SE VA A COMUNICAR CON EL DRIVER DEL LX200
class COM(threading.Thread):
        def __init__(self):
                threading.Thread.__init__(self)
                self.RA=[2,35,54]                              #Variables objetivo a ir
                self.DEC=[89,16,49]                             #Variables objetivo a ir
                self.T=datetime.utcnow()
                self.flag=1
                self.salida=0
                self.botonAL=0
                self.RA_CURR=[2,35,54]
                self.DEC_CURR=[89,16,49]
                self.host = ''
##                self.port = int(sys.argv[1])
##                self.port_indi = int(sys.argv[2])
                self.port = 8000


                ip_get = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                ip_get.connect(("8.8.8.8",80))
                self.host = ip_get.getsockname()[0]
                ip_get.close()

                print(self.host)
                
        def run(self):
  
                
                #Lectura de los botones  
                modo_nivela = ControlMando.control_mando()
                exit_modo = 0

                mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        
##              mySocket.settimeout(0.0)
                
                mySocket.bind((self.host,self.port))

               #Función que lanza el INDI-WEB para poder ejecutar el driver remotamente
                self.server_api()
                
                mySocket.listen(1)



                conn, addr = mySocket.accept()
                print ("connection from: " + str(addr))

 
                
                try:

                        while True:
                                try:

                                        #Función de lectura de los botones
                                        modo_nivela.lectura_botones()
                                        
                                        data = str(conn.recv(1024).decode())
                                        pref = data[1:3]  #prefix // Orden
                                        soc  = data[0]    #Start of com
                                        msg  = data[3:-1] #Message
                                        eoc  = data[-1 ]  #End of com
                                        
                                        print(data)
        ##                                print(pref)
                                        
                                        #Salimos cuando tecleemos cancel
        ##
        ##                                if modo_nivela.cancel == True:
        ##
        ##                                        self.flag = 0
        ##                                        mySocket.close()

                                        #Damos la posición de la recta ascensión donde nos encontramos

                                        if pref == "GR" and soc==':' and eoc=='#':

                                                #Consultamos la RA que nos encontramos

                                                T_RA = self.RA_CURR

                                                T_RA = str(T_RA[0])+':'+str(T_RA[1])+'.'+str(int(T_RA[2]))+'#'

        ##                                      print(T_RA)
                                                
                ##                              T_RA='10:00.0#'

                                                conn.send(str(T_RA).encode())

                                        #Damos la coordenada de declinación donde nos encontramos


                                        elif pref == "GD" and soc==':' and eoc=='#':

                                                
                                                T_DEC = self.DEC_CURR
                                                

                                                T_DEC = str(T_DEC[0])+':'+str(T_DEC[1])+'.'+str(int(T_DEC[2]))+'#'

        ##                                      print(T_DEC)


                ##                              T_DEC='20:00.0#'
                                                                    

                                                conn.send(str(T_DEC).encode())


                                        #Paramos todos los movimientos

                                        elif pref == "Q" and soc==':' and eoc=='#':

                                                print("Deteniendo todos los movimientos del telescopio")


                                        #Sacamos la RA del objeto selecionado en el cielo

                                        elif pref== "Sr" and soc==':' and eoc=='#':

                                                C_RA = msg

        ##                                      print(C_RA)
                                                
                                                #El string recibido es separado y formateado
                                                #y Asignamos el valor a la variable de la clase
                                                strRA=C_RA.replace('.',':').split(':')
                                                if len (strRA)==3:
                                                        self.RA=[int(x) for x in strRA]
                                                        print("Coordenada Ascension Recta Almacenada: " + str(self.RA))
                                                else:
                                                        pass
                                                Answer = str(1)
                                                conn.send(Answer.encode())
                                                Answer=''

                                         #Sacamos la DEC del objeto selecionado en el cielo

                                        elif pref == "Sd" and soc==':' and eoc=='#':

                                                C_DEC = msg

        ##                                      print(C_DEC)
                                                #El string recibido es separado y formateado
                                                #y le asignamos valor a la variable de la clase
                                                strDEC=C_DEC.replace('.',':').split(':')
                                                if len(strDEC)==3:
                                                        self.DEC=[int(x) for x in strDEC]
                                                        print("Coordenada Declinacion Almacenada: " + str(self.DEC))
                                                else:
                                                        pass
        ##                                        Answer = str(1)
        ##                                        conn.send(Answer.encode())
        ##                                        Answer=''

                                        elif pref == "MS" and soc==':' and eoc=='#':

        ##                                      print("Moviendo Telescopio a la coordenada: , " + str(C_RA),str(C_DEC))

                                                Answer = str(0)
                                                conn.send(Answer.encode())
                                                Answer=''
                                                

                                        else:
        ##                                      print("Comando Desconocido")
                                                Answer = str(0)
                                                conn.send(Answer.encode())
                                                Answer=''
                                except:
                                        print('Error COM!!!')
                                        break
                except KeyboardInterrupt:                
                        print('ADIOS COM')
                        mySocket.close()

                print('Saliendo de hilo COM')


        def server_api(self):
                
##                cmd='indi-web -v -P ' + str(self.port_indi) + ' &'
                cmd='indi-web -v &'
##                cmd='indiserver indi_lx200basic &'
                os.system(cmd)
                            
class SW(threading.Thread): ## POSEE TODO LO QUE TIENE QUE VER CON LAS COORDENADAS: RA DEC ALTAZ, FUNCIONESDECAMBIO ETC
        def __init__(self):
                threading.Thread.__init__(self)

                #Hacemos un set con las coordenadas de origen: por defecto Madrid y Polaris
                
                self.C00=COORD.C_00()

                #self.C00.SET_ORI()

                #Establecemos que la coordenada objetivo es Polaris
                self.OBJ=COORD.C_obj(self.C00)

                #Establecemos que el punto donde nos encontramos es Polaris
                self.CURR=COORD.C_Curr(self.C00)
                                
                self.com=COM()
                self.flag=1
                
        def run(self):

                interv=2 
                Tant=0
                
                self.com.start()
                while self.flag:
##                      try:
##                      if self.com.RA != self.OBJ.RA or self.com.DEC != self.OBJ.DEC :

                        Tnow = time.perf_counter()


                        if Tnow>=Tant+interv:
                                print('**')
                                print("-----------")
                                
##                                print(self.OBJ.RA)
##                                print(self.OBJ.DEC)
##
####                                print(self.CURR.ALT)
####                                print(self.CURR.AZ)
####        
##                                print(self.CURR.RA)
##                                print(self.CURR.DEC)
##                                print("-----------")
                                
                                Tant = time.perf_counter()
                        
                        #Recibo del com
                        self.OBJ.RA , self.OBJ.DEC=self.com.RA, self.com.DEC

##                        print(self.OBJ.RA)
##                        print(self.OBJ.DEC)
##

                        T=datetime.utcnow()
##                        T=datetime.utcnow() + timedelta(hours=1)
##                        print(T)

                        #Pasa las coordenadas ecuatoriales (que acabo de recibir del com) absolutas a altacimutales
                        self.OBJ.ACT(T)

                        #Pasa las coordenadas altacimutales a ecuatoriales absolutas (para pasarlas al com)
                        self.CURR.ACT(T)

##                      #Envio al com
##                      print(self.CURR.ALT)
                        
                        self.com.RA_CURR = self.CURR.RA 
                        self.com.DEC_CURR = self.CURR.DEC

##                        print(self.CURR.RA)
##                        print(self.CURR.DEC)
                        

##                      except :
##                              print('Byebye')
##                              break
                self.com.flag=0
                print('Saliendo de hilo SW')
        
############################################################################################################################################################
class MAIN:
        def __init__(self):
                self.flag=1
                self.MODO='M'
                self.BotonAL=0
                self.BotonAZ=0
                self.SW=SW()
                self.HW=HW(c.Enc1A,c.Enc1B,c.Motor1A,c.Motor1B,c.Motor1E,c.CPR1,c.GR1,c.TR_r1,
                           c.Enc2A,c.Enc2B,c.Motor2A,c.Motor2B,c.Motor2E,c.CPR2,c.GR2,c.TR_r2)

                self.lcd = LCD_ON()

                self.modo_nivela = ControlMando.control_mando()
                self.cancel = False
                self.SW.start()
                self.HW.start()


        
        def Set_up(self):     ##LOCALIZACION Y HORA LOCAL

                print("setup")
                
                
##              self.SW.C00.RESET_LOC(self.SW.C00.GdectoGms(40.4086), self.SW.C00.GdectoGms(-3.6922))
                

                #COMPROBACION REGISTRO DE ENTRADAS
                
                exit_triang, Reg_Ciudad, Reg_Lat, Reg_Long, ent_regis =  RegistroEntradas.Comprobacion_Registro(self.lcd)

                #Pasamos las variables que definen nuestro sistema de referencia
                if ent_regis==1:

##                        print(self.SW.OBJ.LAT)
##                        print(self.SW.OBJ.LONG)

                        self.SW.C00.RESET_LOC(self.SW.C00.GdectoGms(float(Reg_Lat)), self.SW.C00.GdectoGms(float(Reg_Long)))

                        self.SW.OBJ.RESET0(self.SW.C00)
                        self.SW.CURR.RESET0(self.SW.C00)
                        
##                        print(self.SW.OBJ.LAT)
##                        print(self.SW.OBJ.LONG)                

##                        print(Reg_Ciudad, Reg_Lat, Reg_Long, ent_regis)
                
                #METODO DE INGRESO DE LA ZONA, HORA Y FECHA ACTUAL
                
                if exit_triang == 0 or ent_regis == 0:

                        #Ejecutamos el código correspondiente al modo seleccionado por el usuario, de esta función sacaremos el modo seleccionado 
                    
                        modo_selec = MenuModo.Selector_Modo(self.lcd)

                        print(modo_selec)
                    
                    
                        #Si el usuario  seleciona el modo automático el sistema se intenterá  conectar a internet para buscar la localización geográfica
                        #y la hora donde se encuentra el individuo de tal forma que no  sea necesario  introducir   ni la latitud y altitud del sitio
                        #ni la hora. En caso de que no se detecte pasará automáticamente al modo manual.
                            
                        if modo_selec == 2:      
                        
                                (status, city, loc) = ModoAutomatico.DescargaDatos(self.lcd)       
                        
                                if status == 'Connected':

                                        print(status, city, loc)
                                        ModoAutomatico.ComprobacionDatos(self.lcd, city, loc)         


                                        #Pasamos las variables que definen nuestro sistema de referencia

##                                        self.SW.C00.GdectoGms(loc[0]), self.SW.C00.GdectoGms(loc[1])
                                        self.SW.C00.RESET_LOC(self.SW.C00.GdectoGms(float(loc[0])), self.SW.C00.GdectoGms(float(loc[1]))) 

                                        self.SW.OBJ.RESET0(self.SW.C00)
                                        self.SW.CURR.RESET0(self.SW.C00)

                                                              
                                        RegistroEntradas.InEntrada(city, loc[0], loc[1])
                            


                                        
                        #Modo manual
                                       
                                       
                        elif modo_selec == 1 or status == 'Not connected':

                                exit_ciudad = 0

                                while not exit_ciudad:
                        
                                        city = ModoManual.Input_Ciudad(self.lcd)
                                    
                                        (lat, long, exit_ciudad) = ModoManual.Busqueda_LatLong(self.lcd,city)

                                        print("FUERA")
                                        print(lat, long, exit_ciudad)
                                    
##                                self.SW.C00.GdectoGms(lat), self.SW.C00.GdectoGms(long)
                                self.SW.C00.RESET_LOC(self.SW.C00.GdectoGms(lat), self.SW.C00.GdectoGms(long))

                                self.SW.OBJ.RESET0(self.SW.C00)
                                self.SW.CURR.RESET0(self.SW.C00)
                                                              
                                #Metemos en el registro la ciudad con su latitud y longitud
                                    
                                RegistroEntradas.InEntrada(city, lat, long)
                                    
                                ModoManual.Input_Hora(self.lcd)
                                        
                                ModoManual.Input_Fecha(self.lcd)

                                print("FIN")
                                
                print ('LAT   ' + str(self.SW.C00.LAT) +'   LONG   ' + str(self.SW.C00.LONG) )
##              
        def Nivelacion_inicial(self):

##              print("NADA")

              #METODO DE NIVELACION DEL TELESCOPIO

              exit_modo = 0
              j=0
                  
              modo_nivel = Nivelacion_Montura.Selec_Nivela(self.lcd)
              
              print(modo_nivel)
              
              while True:

                      if exit_modo == 1:
                              print("Salida del modo de nivelación")
                              break

                      elif modo_nivel == 1:


                              #Accedemos a la función de polaris que nos da las coordenadas de la misma cuando el usuario termine de mover
                              Nivelacion_Montura.Niv_Polaris(self.lcd)
                              self.HW.Modo='M'
                              self.modo_nivela.ok=0

                                
                              while not self.modo_nivela.ok:
  
                                      self.modo_nivela.lectura_botones()

                                      self.BotonAL = self.modo_nivela.modo_funcionamientoAL
                                      self.BotonAZ = self.modo_nivela.modo_funcionamientoAZ

                                      self.HW.Modo = self.MODO
                                      self.HW.BotonAL = self.BotonAL
                                      self.HW.BotonAZ = self.BotonAZ
                                
                        
                                    
                              write_LCD(self.lcd,0,4,'OBJETO',1)
                              write_LCD(self.lcd,1,3,'CENTRADO',0)

                              
                                                
                              file = r'StarDatabase.xls'
                              df = pd.read_excel(file)            

                              long  = len(df['Name'])

                              estrella = 'Polaris'
                                  
                              i = 0

                              for i in range(0,long):
                                      
                                      if estrella == df['Name'][i]:
                                              RA = df['RA'][i]
                                              DEC= df['Dec'][i]
                             
                              print(RA, DEC)

                              millis(0)

                              write_LCD(self.lcd,0,3,'Nivelacion',1)
                              write_LCD(self.lcd,1,2,'correcta V/X',0) 
                 
                              while True:                                
                                      input_state_3 = Lectura_Boton()[2]
                                      input_state_4 = Lectura_Boton()[3]
                                  
                                      if input_state_3 == False:

##                                              print("***************")
##                                              print(self.SW.C00.RA)
##                                              print(self.SW.C00.DEC)
##                                              print("***************")
      
                                              millis(0)
                                              write_LCD(self.lcd,0,3,'Nivelacion',1)
                                              write_LCD(self.lcd,1,2,'completada',0)

                                              #Provocamos un reseteo manual de la placa para leer desde el principio
                                              self.HW.SER.setDTR(False)
                                              self.HW.SER.flushInput()
                                              self.HW.SER.setDTR(True)

                                              time.sleep(2)


                                              #Pasar las variables de RA y DEC con el siguiente formato

                                              RA_1 = RA[0:2]
                                              RA_2 = RA[3:5]
                                              RA_3 = RA[6:8]

##                                              RA_F = (int(RA_1),int(RA_2),int(RA_3))
                                              RA_F = [int(RA_1),int(RA_2),int(RA_3)]

                                              DEC_1 = DEC[1:3]
                                              DEC_2 = DEC[4:6]
                                              DEC_3 = 0

                                              DEC_F = (int(DEC_1),int(DEC_2),int(DEC_3))
                                              DEC_F = [int(DEC_1),int(DEC_2),int(DEC_3)]


##                                            print(RA_1,RA_2,RA_3)                                           
##                                            print(DEC_1,DEC_2,DEC_3)
##
                                              print(DEC_F)                                            
                                              print(RA_F)
                                              
                                              #Pasamos las variables de RA y DEC para establecerlas como origen del telecopio

                                              self.SW.C00.RESET_ORI(RA_F,DEC_F)

                                              self.SW.OBJ.RESET0(self.SW.C00)
                                              self.SW.CURR.RESET0(self.SW.C00)

##                                              print("***************")
##                                              print(self.SW.C00.RA)
##                                              print(self.SW.C00.DEC)
##                                              print("***************")
                                              
                                              exit_modo = 1
                                      
                                              break
                                  
                                      elif input_state_4 == False:
                                              break
                                     
##                      elif modo_nivel == 2:
##
##                              #Creamos vectores para almacenar las estrellas recogidas
##                              
##                      
##                              for j in range(0,3):
##                                  
##                                      #LLamada a la función que nos saca el valor de la RA y DEC de las tablas que tenemos de estrellas como base de datos
##                                      write_LCD(self.lcd,0,3,'NIVELE EL',1)  
##                                      write_LCD(self.lcd,1,1,'TELESCOPIO',0)  
##
##                                      millis(0)
##                                  
##                                      write_LCD(self.lcd,0,1,'SELECCIONE UNA',1)
##                                      write_LCD(self.lcd,1,4,'ESTRELLA',0)
##                                  
##                                  
##                                      #Llamada a la función para registrar las estrellas con la que vamos a nivelar
##                                      N_Estrella, Coord_RA, Coord_DEC=Nivelacion_Montura.Selector_Estrellas(self.lcd)
##
##                                      #Pasar las variables de RA y DEC con el siguiente formato
##
##                                      RA_1 = Coord_RA[0:2]
##                                      RA_2 = Coord_RA[3:5]
##                                      RA_3 = Coord_RA[6:8]
##
##                                      #RA_F = (int(RA_1),int(RA_2),int(RA_3))
##                                        RA_F = [int(RA_1),int(RA_2),int(RA_3)]
##
##                                      DEC_1 = Coord_DEC[1:3]
##                                      DEC_2 = Coord_DEC[4:6]
##                                      DEC_3 = 0
##
##                                      #DEC_F = (int(DEC_1),int(DEC_2),int(DEC_3))
##                                      DEC_F = [int(DEC_1),int(DEC_2),int(DEC_3)]
##
##                                      print("NADANDAFASDFS")
##                                      print(j)
####                                    Nivelacion_Montura.Niv_TresEstrellas(self.lcd,j)
##
##                                      #Seleccionamos MODO MANUAL, MODO AUTOMÁTICO O MIXTO DEPENDIENDO LA ESTRELLA QUE REALICEMOS
##
##
##                                      if j==0:
##
##                                              #La primera estrella que cojamos la tomara como origen y el movimiento será en manual para llegar hasta ella
##                                              millis(0)
##                                              
##                                              write_LCD(self.lcd,0,1,'UNA VEZ LLEGUE',1)
##                                              write_LCD(self.lcd,1,5,'PULSE V',0)
##
##                                              while not self.modo_nivela.ok:
##                
##                                                      self.modo_nivela.lectura_botones()
##
##                                                      self.HW.MALTURA.MOT.SP = (60*self.modo_nivela.modo_funcionamientoAL)
####                                                      self.HW.MAZIMUT.MOT.SP = (60*self.modo_nivela.modo_funcionamientoAZ)
##
####                                                        
####                                                #Llamada a la clase que regula la lectura de los botones para que se muevan en una dirección sin PWM
####                                                modo_nivela = ControlMando.control_mando()    
####                                                modo_nivela.lectura_botones()
####
##                                          
##                                              write_LCD(self.lcd,0,4,'OBJETO',1)
##                                              write_LCD(self.lcd,1,3,'CENTRADO',0)
##
##                                              #Pasamos las variables de RA y DEC para establecerlas como origen del telecopio
##
##                                              self.SW.C00.RESET_ORI(RA_F,DEC_F)
##
##                                              #Establecemos que los pasos en este punto son 0
##
##                                              self.HW.MALTURA.ENC.P = 0
##                                              self.HW.MAZIMUT.ENC.P = 0
##
##                                              #Pasamos las variables RA y DEC como las coordenadas actuales del telescopio
##                                                     
##                                              self.SW.CURR.RA = RA_F
##                                              self.SW.CURR.DEC = DEC_F
##                                                                                              
##                                                  
##                                      elif j==1:
##                                              #La segunda estrella tendrá un movimiento en automático y se corregirá luego con el modo manual de movimiento
##                                              millis(0)
##                                              
##                                              write_LCD(self.lcd,0,3,'MOVIENDO',1)
##                                              write_LCD(self.lcd,1,1,'AUTOMATICAMENTE',0)
##
##                                              #Introducción de la función que regula el proceso automático de movimiento
##                                              #Falta la función a la que hay que darle las coordenadas del movimiento automatico
##                                              #Es necesario una variable para detectar cuadno se ha parado el movimiento automático
##
##                                              
##                                              self.MODO = 'A'
##
##                                              #Pasamos las variables de RA y DEC para establecerlas como destino del telecopio
##
##                                              self.SW.OBJ.RA = RA_F
##                                              self.SW.OBJ.DEC = DEC_F
##
##
##                                              #Ejecutamos la función del modo automático. Se mueve hasta que el error sea menor que XXX
##
##                                              while True:
##                                                      
##                                                      self.CALCULOS()
##                                              
##                                              millis(0)       
##                                              
##                                              write_LCD(self.lcd,0,3,'AJUSTAR',1)
##                                              write_LCD(self.lcd,1,1,'MANUALMENTE',0)
##
##                                              
##                                              self.MODO = 'M'                                             
##
##                                              while not self.modo_nivela.ok:
##                
##                                                      self.modo_nivela.lectura_botones()
##
##                                                      self.HW.MALTURA.MOT.SP = (60*self.modo_nivela.modo_funcionamientoAL)
####                                                      self.HW.MAZIMUT.MOT.SP = (60*self.modo_nivela.modo_funcionamientoAZ)     
##                                          
##
##                                              write_LCD(self.lcd,0,4,'OBJETO',1)
##                                              write_LCD(self.lcd,1,3,'CENTRADO',0)
##
##
##                                              #Variables corregidas
##
##                                              RA_F = RA_F_CORR
##                                              DEC_F = DEC_F_CORR
##
##                                              
##                                              #Pasamos las variables RA y DEC como las coordenadas actuales del telescopio
##                                              #Coordenadas corregidas
##                                                     
##                                              self.SW.CURR.RA = RA_F
##                                              self.SW.CURR.DEC = DEC_F    
##                                                  
##                                                  
##                                      elif j==2:
##
##                                              #La tercera estrella tendrá un movimiento completamente en automático ya que el movimiento estaría corregido, si no se llegara al valor correcto se debería rehacer el proceso
##                                              millis(0)
##                                              
##                                              write_LCD(self.lcd,0,3,'MOVIENDO',1)
##                                              write_LCD(self.lcd,1,1,'AUTOMATICAMENTE',0)
##
##                                              #Pasamos las variables de RA y DEC para establecerlas como destino del telecopio
##
##                                              self.SW.OBJ.RA = RA_F
##                                              self.SW.OBJ.DEC = DEC_F
##
##                                              #Introducción de la función que regula el proceso automático de movimiento
##                                              #Falta la función a la que hay que darle las coordenadasd el movimiento automático
##                                              #Es necesario una variable para detectar cuadno se ha parado el movimiento automático
##                                              
##                                              self.MODO = 'A'
##
##                                              #Ejecutamos la función del modo automático. Se mueve hasta que el error sea menor que XXX
##
##                                              while True:
##                                                      
##                                                      self.CALCULOS()
##                                                      
##                                              
##                                              #Pasamos las variables RA y DEC como las coordenadas actuales del telescopio
##                                                     
##                                              self.SW.CURR.RA = RA_F
##                                              self.SW.CURR.DEC = DEC_F
##                                                  
####                                            millis(0)
####
####                                            
####                                            while True:
####                                               
####                                                    input_state_3 = Lectura_Boton()[2]
####                                                
####                                                    if input_state_3 == False:
####                                                            write_LCD(self.lcd,0,4,'OBJETO',1)
####                                                            write_LCD(self.lcd,1,3,'CENTRADO',0)
####                                                            break
##                                                  
##
##
##                              #Comprobación final si la nivelación se ha realizado correctamente
##       
##                              millis(0)
##
##                              write_LCD(self.lcd,0,3,'Nivelacion',1)
##                              write_LCD(self.lcd,1,2,'correcta V/X',0) 
##         
##                              while True:
##                          
##                                      input_state_3 = Lectura_Boton()[2]
##                                      input_state_4 = Lectura_Boton()[3]
##                          
##                                      if input_state_3 == False:
##                                              millis(0)
##                                              write_LCD(self.lcd,0,3,'Nivelacion',1)
##                                              write_LCD(self.lcd,1,2,'completada',0)
##                                              exit_modo = 1
##                                              
##                                              break
##                                          
##                                      elif input_state_4 == False:
##                                              break
        

        def Modos_Finales (self):
                        
                 
                menu_1 = ['MODO MANUAL', 'MODO KSTARS']
                menu_2 = ['MODO  MANUAL\n\r SELECCIONADO','MODO KSTARS\n\r SELECCIONADO']
                cont_1 = 0
                
                #Variable que  nos permitirá conocer el modo que el usuario ha seleccionado    
                modo_selec = 0         
                   
                #Selección de menú
                

                write_LCD(self.lcd,0,3,'Selector de',1)
                write_LCD(self.lcd,1,6,'Modo',0)
                    

                while True:
                    
                    
                        input_state_1 = Lectura_Boton()[0]

                        
                        if input_state_1 == False:                          
   
                                write_LCD(self.lcd,0,0,str(menu_1[cont_1]),1)

                                millis(0)


                        
                                while True:
                                        input_state_1 = Lectura_Boton()[0]
                                        input_state_3 = Lectura_Boton()[2]
                                    
                                        if input_state_1 == False:
                                        
                                                if cont_1 >= 1:
                                                        break
                                                           
                                                else:
                                                        cont_1 = cont_1 + 1
                                                        print(cont_1)

                                                        write_LCD(self.lcd,0,0,str(menu_1[cont_1]),1)
                                            
                                                        millis(0)

                                                        while True:
                                                                input_state_1 = Lectura_Boton()[0]
                                                                input_state_2 = Lectura_Boton()[1]
                                                                input_state_3 = Lectura_Boton()[2]


                                                                if input_state_2 == False:
                                                                        
                                                                        if cont_1 == 0:
                                                                    
                                                                                millis(0)                                                           
                                                                                break
                                                                        
                                                                        else:
                                                                                cont_1  = cont_1 - 1
                                                                                    
                                                                                write_LCD(self.lcd,0,1,str(menu_1[cont_1]),1)

                                                                                break

                                                                                
                                                                    
                                                                elif input_state_1 == False:
                                                                        cont_1 = 0
                                                                        break
                                                                
                                                                elif input_state_3 == False:

                                                                    
                                                                        write_LCD(self.lcd,0,1,str(menu_2[1]),1)                                      
                                                                
                                                                        #Asignamos a la variable dentor de la clase el valor para saber el modo
                                                                        self.MODO = 'A'
                                                                        
                                                                        #Llamada a la función de comunicación con el cliente externo
                                                                        
##                                                                      self.com.start()

                                                                        modo_selec = 1

                                                                        break
                                                                
                                        
                                        elif modo_selec != 0:
                                                break
##                                                
                                        elif input_state_3 == False:
                                                                    
                                                write_LCD(self.lcd,0,3,str(menu_2[0]),1)
                                            
                                                #Asignamos a la variable el valor para saber el modo                        
                                            
                                                self.MODO = 'M'

                                                modo_selec = 1

                                                break

                        elif modo_selec != 0:
                                break

        
        def Proceso (self):

##                #Ejecutamos funciones generales como la del LCD y configura los pines de los botones
##
                Button_State()

               #SET DE HORA, FECHA, LOCALIZACIÓN INCIAL  ----- COMPROBADA


##                self.Set_up() ##Set HORA FECHA ETC
##
##                print(self.SW.C00.LAT, self.SW.C00.LONG)
##
##
##                #SET DE RA, DEC INICIALES, CON CORRECCIÓN (MÉTODO TRES ESTRELLAS) O SIN ELLA (MÉTODO POLARIS)
##
##                self.Nivelacion_inicial() ##(polaris tres etstrellas etc)-cuando Nivelacion_inicial() acabe hay un origen

        def CALCULOS(self):


                #PASO DE PASOS DADOS A GRADOS RECORRIDOS
                self.SW.CURR.GALT= self.HW.MALTURA.TRtot*self.HW.MALTURA.ENC.P           ##TRANSFORMA PASOS A GRADOS Y SE LOS PASA AL EJE CORRESPONDIENTE
                self.SW.CURR.GAZ = self.HW.MAZIMUT.TRtot*self.HW.MAZIMUT.ENC.P            ##DE LA COORDENADA ACTUAL DEL TELESCOPIO
                
                #SUMA LOS GRADOS RECORRIDOS A LAS COORDENADAS INICIALES

                cGALT0=self.SW.C00.GmstoGdec(self.SW.C00.ALT) ##Coordenada del origen en grados para sumarla
                CURRg=cGALT0+self.SW.CURR.GALT                   ##Suma los grados del origen y los dados y haya los grados recorridos
                self.SW.CURR.ALT=self.SW.CURR.GdectoGms(CURRg)   ##Paso de grados a GMS

                cGAZ0=self.SW.C00.GmstoGdec(self.SW.C00.AZ) ##Coordenada del origen en grados para sumarla
                CURRg=cGAZ0+self.SW.CURR.GAZ                   ##Suma los grados del origen y los dados y haya los grados recorridos
                self.SW.CURR.AZ=self.SW.CURR.GdectoGms(CURRg)   ##Paso de grados a GMS
                
 
                #PASO A GDEC EL OBJETIVO Y EL ORIGEN PARA RESARLO
                ALTg=self.SW.OBJ.GmstoGdec(self.SW.OBJ.ALT)
                AZg=self.SW.OBJ.GmstoGdec(self.SW.OBJ.AZ)

                ALT0g=self.SW.C00.GmstoGdec(self.SW.C00.ALT)
                AZ0g=self.SW.C00.GmstoGdec(self.SW.C00.AZ)
    
                #PASAMOS EL EL MODO AL ARDUINO PARA QUE SE MUEVAN LOS MOTORES Y LA DIRECCIÓN EN EL MODO MANUAL 
                self.HW.Modo = self.MODO
                self.HW.BotonAL = self.BotonAL
                self.HW.BotonAZ = self.BotonAZ

                #DIFERENCIAMOS ENTRE EL MODO MANUAL Y EL MODO AUTOMÁTICO

                
                if self.MODO=='A':

##                        interv=2
##                        Tant=0
                        
##                      self.HW.MALTURA.MOT.SP=self.HW.MALTURA.pid
##                        self.HW.MAZIMUT.MOT.SP=self.MAZIMUT.pid
##
##
##                        Tnow = time.perf_counter()
##
##                        if Tnow>=Tant+interv:
##                        print("/////////////////////")
##                        print(self.HW.MALTURA.ENC.P)
##                        print(self.HW.MAZIMUT.ENC.P)
##                        print(self.HW.MALTURA.MOT.SP)
##                        print(self.HW.MAZIMUT.MOT.SP)
##                        print(ALTg)
##                        print(ALT0g)
##                        print(self.HW.MALTURA.TRtot)
##                        print("/////////////////////")
##
##                                Tant = Tnow
##

                        sp_alt = (ALTg-ALT0g)/(self.HW.MALTURA.TRtot) 
                        sp_azi = (AZg-AZ0g)/(self.HW.MAZIMUT.TRtot)
                        print(sp_alt)
                        print(sp_azi)
                        if sp_alt >= self.HW.MALTURA.TRtot:
                                

                                self.HW.MALTURA.MOT.SP = sp_alt - self.HW.MALTURA.TRtot
                        if sp_alt < 0:
                                

                                self.HW.MALTURA.MOT.SP = sp_alt + self.HW.MALTURA.TRtot

                        if sp_azi >= self.HW.MAZIMUT.TRtot:
                                

                                self.HW.MAZIMUT.MOT.SP = sp_azi - self.HW.MAZIMUT.TRtot
                        if sp_azi < 0:
                                

                                self.HW.MAZIMUT.MOT.SP = sp_azi + self.HW.MAZIMUT.TRtot

                        print(self.HW.MAZIMUT.MOT.SP)
                        print(self.HW.MAZIMUT.MOT.SP)
                        
                       
                        
                        self.modo_nivela.lectura_botones()
                        self.cancel = self.modo_nivela.cancel                        

                        
##                      print('ALT OBJ:   '+ str(self.SW.OBJ.ALT)+'\t AZ OBJ   '+str(self.SW.OBJ.AZ))
##                      print('ALT ACT:   '+ str(self.SW.CURR.ALT)+'\t AZ ACT   '+str(self.SW.CURR.AZ))
                        
##                      print('RA OBJ:   '+ str(self.SW.OBJ.RA)+'\t DEC OBJ   '+str(self.SW.OBJ.DEC))                   
##                      print('RA ACT:   '+ str(self.SW.CURR.RA)+'\t DEC ACT   '+str(self.SW.CURR.DEC))
##                      ACTH , ACTDECh=self.SW.CURR.ALTAZtoEQh(self.SW.CURR.LAT,self.SW.CURR.ALT,self.SW.CURR.AZ)
##                      print(ACTH)
##                      print(ACTDECh)
##                      ACTRA, ACTDEC =self.SW.CURR.EQhtoEQabs(datetime.utcnow(),self.SW.CURR.LAT,self.SW.CURR.LONG,ACTH,ACTDECh)
##                      print(ACTRA)
##                      print(ACTDEC)
                        
                if self.MODO=='M':

##                        print(self.HW.MALTURA.ENC.P)
##                        print(self.HW.MAZIMUT.ENC.P)                        
                     
                        self.modo_nivela.lectura_botones()

                        self.BotonAL = self.modo_nivela.modo_funcionamientoAL
                        self.BotonAZ = self.modo_nivela.modo_funcionamientoAZ
                        
                        self.cancel = self.modo_nivela.cancel
                


        def TLSCP(self):

                #EJECUTAMOS EL MENU DE MODO NORMAL PARA CAMBIAR ENTRE MOVIMIENTO MANUAL O AUTOMÁTICO (A TRAVÉS DE KSTARS)


                while True:

##                      millis(0)
##
##                      #Leemos el valor del botón cancel
##                      
##                      self.modo_nivela.lectura_botones()                       
##                      self.cancel = self.modo_nivela.cancel
                        
##                      print("Entrada en el Menu")

                        self.Modos_Finales()
##                      self.MODO='M'

##                      print("Salida del menu")


                        interv=0.02  
                        Tant=0

                        self.cancel = 0
                        
                        while not self.cancel:
                                try:
                                        Tnow=time.perf_counter()
##                                      self.MODO=self.SW.com.MODO
##                                      self.BotonAL=self.SW.com.botonAL
##                                      self.BotonAL=self.SW.tecla.Direccion
                                        

                                        if self.SW.com.salida : self.flag=0
                                        if Tnow>=Tant+interv:
                                                Tant=Tnow
                                                self.CALCULOS()
##                                              self.DEFINIR_MODOS()

                                                
                                                
                                except KeyboardInterrupt:
                                        print('BYE')
##                                      self.flag=0
##                      self.SW.com.flag=0
##                      self.SW.flag=0
##      ##                self.SW.tecla.flag=0
##                      self.SW.com.join()
##                      self.SW.join()
##                    
##                      self.HW.MALTURA.flag=0
##      ##                self.HW.MAZIMUT.flag=0
##                    
##                      time.sleep(1)
##                      self.HW.MALTURA.join()
##      ##                self.HW.MAZIMUT.join()
##                      print('Saliendo de hilo MAIN')


##                      
##      def DEFINIR_MODOS(self):
##              if self.MODO=='A':
####                    self.HW.MALTURA.MOT.SP=self.HW.MALTURA.pid
####                        self.HW.MAZIMUT.MOT.SP=self.MAZIMUT.pid
##
##                      self.HW.MALTURA.MOT.SP = self.HW.MALTURA.ENC.P - (ALTg-ALT0g)/(self.HW.MALTURA.TR_r)
##                      self.HW.MAZIMUT.MOT.SP = self.HW.MAZIMUT.ENC.P - (AZg-AZ0g)/(self.HW.MAZIMUT.TR_r)
##
####                      print('ALT OBJ:   '+ str(self.SW.OBJ.ALT)+'\t AZ OBJ   '+str(self.SW.OBJ.AZ))                   
####                      print('ALT ACT:   '+ str(self.SW.CURR.ALT)+'\t AZ ACT   '+str(self.SW.CURR.AZ))
##                      
####                      print('RA OBJ:   '+ str(self.SW.OBJ.RA)+'\t DEC OBJ   '+str(self.SW.OBJ.DEC))                   
##                      print('RA ACT:   '+ str(self.SW.CURR.RA)+'\t DEC ACT   '+str(self.SW.CURR.DEC))
####                      ACTH , ACTDECh=self.SW.CURR.ALTAZtoEQh(self.SW.CURR.LAT,self.SW.CURR.ALT,self.SW.CURR.AZ)
####                      print(ACTH)
####                      print(ACTDECh)
####                      ACTRA, ACTDEC =self.SW.CURR.EQhtoEQabs(datetime.utcnow(),self.SW.CURR.LAT,self.SW.CURR.LONG,ACTH,ACTDECh)
####                      print(ACTRA)
####                      print(ACTDEC)
##                      
##              if self.MODO=='M':
##                   
##                      self.modo_nivela.lectura_botones()
##
##                      self.BotonAL = self.modo_nivela.modo_funcionamientoAL
##                      self.BotonAZ = self.modo_nivela.modo_funcionamientoAZ
##                      
##                      self.cancel = self.modo_nivela.cancel
##                      
####                    print('MODO  '+str(self.MODO)+ '  BOTON  '+str(self.BotonAL))
####                    print('PASOS '+str(self.HW.MALTURA.ENC.P)+'  CANAL A  '+str(self.HW.MALTURA.ENC.CA)+'  CANAL B  '+str(self.HW.MALTURA.ENC.CB))
####            

################################################################################
                                
#Datos propios de la observación
Hora=[1,45,20]
Fecha=[2018,3,19]

Time=datetime(*(Fecha+Hora))

Time=datetime.utcnow()

LAT_M=[40,24,59]#gms
LONG_M=[-3,-42,-9]

#Datos propios del astro origen
AR_pol=[2,35,54]#hms
DEC_pol=[89,16,49]#gms

#Datos propios del astro observado
AR_Acent=[14,39,36.5]
DEC_Acent=[-60,-50,-13.8] ##CUIDADO LOS NEGATIVOS SON TODOS NEGATIVOS!!
print(Time)




M=MAIN()
print('*******')



##
##M.SW.C00.RESET_ORI(AR_pol,DEC_pol)
##M.SW.C00.RESET_LOC(LAT_M,LONG_M)

print('LAT: '+str(M.SW.C00.LAT)+' LONG: '+str(M.SW.C00.LONG))

M.SW.OBJ.RA=AR_pol
M.SW.OBJ.DEC=DEC_pol



M.Proceso()

M.TLSCP()





    
                
