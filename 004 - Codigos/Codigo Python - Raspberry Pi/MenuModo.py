#!/usr/bin/python3


#Importamos las librerias necesarias

import sys
import time
import re
import time
import os


from FuncionesGenerales import LCD_ON, write_to_lcd, loop_string, Button_State, Lectura_Boton, millis, write_LCD


#LLAMADAS A FUNCIONES GENERALES USADAS A LO LARGO DEL SCRIPT

#Configuracion Botones

Button_State()


#FUNCIÓN PRINCIPAL

def Selector_Modo(lcd):

    #VARIABLES LOCALES
    
    menu_1 = ['MODO MANUAL', 'MODO AUTOMATICO']
    menu_2 = ['MODO  MANUAL\n\r SELECCIONADO','MODO AUTOMATICO\n\r SELECCIONADO']
    cont_1 = 0
    
    #Variable que  nos permitirá conocer el modo que el usuario ha seleccionado    
    modo_selec = 0  
        
    #Contadores de tiempo
    framebuffer = [
            '',
            '',
            ]
        
    write_LCD(lcd,0,1,'Control manual\n\r telescopio DIY',1)

    millis(0)
        
    #Llamada a la función encargada de la escritura en pantalla      
                                  
    #write_to_lcd(lcd, framebuffer, 16)

    #Frase inicial de bienvenida
    long_string = 'Bienvenido al programa de control manual del Telescopio DIY desarrollado por Jose Maria Nieto y Daniel Del Mazo'
                
    #Función que realiza el scrolling
                
    #loop_string(long_string, lcd, framebuffer, 1, 16)
         

        
    #Selección de menú
    

    write_LCD(lcd,0,3,'Selector de',1)
    write_LCD(lcd,1,6,'Modo',0)
        
    while True:
        
        
        input_state_1 = Lectura_Boton()[0]

            
        if input_state_1 == False:    
                
            lcd.clear()
            lcd.cursor_pos = (0,3)
            lcd.write_string(menu_1[cont_1])          
                
            millis(0)
            
            while True:
                    input_state_1 = Lectura_Boton()[0]
                    input_state_3 = Lectura_Boton()[2]
                        
                    if input_state_1 == False:
                            
                        if cont_1 >= 1:
                            break
                                               
                        else:
                            cont_1 = cont_1 + 1
                            print(cont_1)

                            write_LCD(lcd,0,0,str(menu_1[cont_1]),1)
##                            print("1")
                            
                            millis(0)

                            while True:
                                input_state_1 = Lectura_Boton()[0]
                                input_state_2 = Lectura_Boton()[1]
                                input_state_3 = Lectura_Boton()[2]
                                        
                                if input_state_2 == False:
                                            
                                    if cont_1 == 0:
                                        
                                        millis(0)
                                                
                                        break
                                            
                                    else:
                                        cont_1  = cont_1 - 1
                                                
                                        write_LCD(lcd,0,1,str(menu_1[cont_1]),1)
##                                        print("2")
                                        break
                                        
                                elif input_state_1 == False:
                                    cont_1 = 0
                                    break
                                    
                                elif input_state_3 == False:
                                        
                                    write_LCD(lcd,0,1,str(menu_2[1]),1)
##                                    print("3")
                                    modo_selec = 2
##                                    print(modo_selec)
                                    
                                    millis(0)

                                    break
                            
                    elif modo_selec != 0:
                        break
                            
                    elif input_state_3 == False:
                                                
                        write_LCD(lcd,0,3,str(menu_2[0]),1)
##                        print("4")
                        modo_selec = 1
                                
                        millis(0)                   

                        break

        elif modo_selec != 0:
            break
                    
        elif modo_selec == 1 or modo_selec == 2:
            break

    return modo_selec
