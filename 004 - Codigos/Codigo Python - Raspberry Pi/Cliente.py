#!/usr/bin/python3


#Importamos las librerias necesarias

import sys
import time
import socket
import threading

def client():

    if len (sys.argv) != 3:
        print("Usage:p python3 client.py IP PORT")
        sys.exit(1)

    host = sys.argv[1]
    port = int(sys.argv[2])

    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mySocket.connect((host,port))


client()
