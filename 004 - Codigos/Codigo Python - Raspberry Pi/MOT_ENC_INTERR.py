# -*- coding: utf-8 -*-
#!/usr/bin/python3

#Crea un hilo maestro por cada motor encoder, este hilo abre otros dos sub-hilos;
#Uno para el motor y otro para el encoder; mientras el hilo principal recibe
# todos los datos y a través del PID cambia la señal de control del motor

import threading
import time
import sys
import socket
import RPi.GPIO as GPIO



GPIO.setwarnings(False)
GPIO.cleanup()
class Mot:#(threading.Thread):
	def __init__(self,A, B, E):
		#threading.Thread.__init__(self)
		self.A=A
		self.B=B
		self.E=E
		self.SP=0
		self.SP_ant=0
		GPIO.setmode(GPIO.BOARD)
		GPIO.setup(self.A,GPIO.OUT)
		GPIO.setup(self.B,GPIO.OUT)
		GPIO.setup(self.E,GPIO.OUT)
		self.pwm=GPIO.PWM(self.E,7000) ##Frecuencia PWM
		self.pwm.start(0)

		self.flag=1
	def setSP(self):
		interv=0.1
		Tnow=0
		Tant=0

		Tnow=time.perf_counter()
		
		if (Tnow >= Tant + interv) and self.SP_ant!=self.SP :
			print('cambio')
			Tant=Tnow
			if abs(int(self.SP))>=100:
				self.SP=abs(self.SP)/(self.SP)*100
			if self.SP<0:
				self.BWDpwm(abs(self.SP))
			if self.SP>0:
				self.FWDpwm(abs(self.SP))
			if self.SP==0:
				self.stopM()
		else: pass
		
##		print('Saliendo de cambio MOTOR')
	def FWDpwm(self,Dcycle):
		GPIO.setmode(GPIO.BOARD)
		self.pwm.ChangeDutyCycle(Dcycle)
##		self.pwm.start(Dcycle)
		
		#print('FWD--Dcycle '+str(self.SP))
		GPIO.output(self.A,GPIO.HIGH)
		GPIO.output(self.B,GPIO.LOW)
		GPIO.output(self.E,GPIO.HIGH)
	def BWDpwm(self,Dcycle):	
		GPIO.setmode(GPIO.BOARD)
		self.pwm.ChangeDutyCycle(Dcycle)
##		self.pwm.start(Dcycle)	
		
		#print('BWD--Dcycle '+str(self.SP))
		GPIO.output(self.A,GPIO.LOW)
		GPIO.output(self.B,GPIO.HIGH)
		GPIO.output(self.E,GPIO.HIGH)
	def stopM(self):
		#GPIO.output(self.A,GPIO.LOW)
		#GPIO.output(self.B,GPIO.LOW)
		#GPIO.output(self.E,GPIO.LOW)
		self.pwm.stop()
	def mstoDutyCycle(self, ms):
		Maxpwm=19200
		Minpwm=500
		MD=100
		mD=0
		if ms<Minpwm: Dcycle=0
		elif ms>Maxpwm: Dcycle=100
		else: Dcycle= (MD-mD)*(ms-Minpwm)/(Maxpwm-Minpwm)
		#print(str(Dcycle))
		return Dcycle

class Enc:#(threading.Thread):

	def __init__(self, PCA,PCB): #pines de los canales A y B
		#threading.Thread.__init__(self)

		self.PCA=PCA #Pin del canal A del motor ##ESTATICOS
		self.PCB=PCB #Pin del canal B del motor
		
		GPIO.setmode(GPIO.BOARD)
		GPIO.setup(self.PCA,GPIO.IN)
		GPIO.setup(self.PCB,GPIO.IN)
		
		self.CA= GPIO.input(self.PCA) #estado del canal A del motor
		self.CB= GPIO.input(self.PCB) #estado del canal B del motor	
		
		self.AB=1
		self.AB_ant=1
		self.Mat=[[0, 1 ,-1, 'E'],[-1 ,0, 'E', 1],[1, 'E', 0 ,-1 ],['E', -1 ,1, 0]] #Matriz encoder		
		
		self.P_time=20*[[0,0]]
		self.P=0 #Pulsos desde el cero :distancia del objetivo al cero en pasos
		
		#self.pwm=50000	
		self.Sentido=1		       		
		self.I=0
		#self.lock=threading.Lock()
		self.flag=1
		self.t0=time.perf_counter()

		
	def Set_interr(self):
		try:
			print('Establecimiento interrupciones')
			GPIO.add_event_detect(self.PCA, GPIO.BOTH)
			GPIO.add_event_callback(self.PCA,self.Cuenta_Matriz1)
			GPIO.add_event_detect(self.PCB, GPIO.BOTH)
			GPIO.add_event_callback(self.PCB,self.Cuenta_Matriz1)
			print('Establecidas!')
		except:
			print('OH .OH....')
		
	def Cuenta_Matriz1(self,PCA):
##		print('ISR A')
		try:
			GPIO.setmode(GPIO.BOARD)
			
			self.CA= GPIO.input(self.PCA)
			
			self.CB= GPIO.input(self.PCB)

			##LOCK!!!!!!!!!!!!
			
			self.AB_ant=self.AB
			self.AB=(self.CA<<1)+self.CB
			inc=self.Mat[self.AB_ant][self.AB]
			
			if inc=='E':
				print('Error Contador!!')
				inc=2
			 
				
			self.P+=inc
		except:
			print('Error interr')
			pass
	

	
	def Cuenta_Matriz2(self,t0):
		GPIO.setmode(GPIO.BOARD)

		
		CA_ant=self.CA
		CB_ant=self.CB
		
		self.CA= GPIO.input(self.PCA)
		self.CB= GPIO.input(self.PCB)
		
		if CA_ant !=self.CA:
			incA=1
		else: incA=0
		
		if CB_ant !=self.CB:
			incB=1
		else: incB=0

		incA*=self.Sentido
		incB*=self.Sentido
		self.tnow=time.perf_counter()-t0
		self.P+=incA
		self.P_time.append([self.P, self.tnow])
		del(self.P_time[0])
		
class MOTENC(threading.Thread):
	def __init__(self,PCA,PCB,A,B,E,CPR,GR,TR_r):
		threading.Thread.__init__(self)
		self.MOT=Mot(A,B,E)
		self.ENC=Enc(PCA,PCB)


		
		self.pid=43

		self.ERROR=0
		self.CPR=CPR
		self.GR=GR
		self.TR_r=TR_r
		self.flag=1
		self.TRtot=(360)/(self.CPR*self.GR*TR_r)
		##grados por paso
	def run(self):
		#self.MOT.SP=280
		self.ENC.Set_interr()
		I=0
		Tnow=0
		Tant=0
		interv=1
##		self.MOT.start()
##		self.ENC.start()
		while self.flag:
			try:
				Tnow=time.perf_counter()
				if Tnow>=Tant+interv:
					Tant=Tnow
					self.MOT.SP_ant=self.MOT.SP

					P20=self.ENC.P_time[-20][0]
					t20=self.ENC.P_time[-20][1]
					P0=self.ENC.P_time[-1][0]
					t0=self.ENC.P_time[-1][1]
					#print('PIDDDDDDD')
					self.pid,self.ERROR,self.I=self.PID(  P20,t20,P0,t0, I)
		
##					self.MOT.SP=int(input('SP::   '))
					self.MOT.SP=2
					self.MOT.setSP()
					
##					#self.MOT.SP=int(self.MOT.SP)
##					if not self.MOT.SP: self.ENC.Sentido=1
##					else : self.ENC.Sentido=self.MOT.SP/abs(self.MOT.SP)
			except Exception as exc:
				
				print('Error',exc)
				self.flag=0
		self.MOT.flag=0
##		self.ENC.flag=0
##		self.MOT.join()
##		self.ENC.join()
##		self.join()
		print('Saliendo de hilo MOT_ENC')
	def PID(self,P20,t20,P0,t0,I):
		#w=(P_time[-1][0]-P_time[-20][0])/(P_time[-1][1]-P_time[-20][1])#v de giro (w) en pulsos/seg
		dp=P0-P20
		dt=t0-t20
		if dt : v20=dp/dt
		else :  v20=0
		
		err=-self.ERROR
		#print(err)
		maxI=10
		infI=-maxI
		maxpwm=2000
		minpwm=0
		Kp, Ki, Kd= 1 , 0.08 ,0.1
		P=Kp*err
		I+=Ki*err*dt
		D=Kd*v20
		if abs(err)<200:  I=0
		elif abs(I)>=maxI:I=maxI
		PID=round(P+I+D)
##		if PID>=maxpwm: PID=maxpwm
##		elif PID<=minpwm: PID=minpwm
		if abs(PID)>=100: PID=abs(PID)/(PID)*100
##		elif PID<=0: PID=0
		if err==0: PID=0

		return PID , err ,I  
		
				

	
##
Enc1A=11
Enc1B=13
##
##Enc2A=9
##Enc2B=10
##
Motor1A = 18
Motor1B = 16
Motor1E = 12
## 
##Motor2A = 5
##Motor2B = 6
##Motor2E = 13
CPR=6
GR=297.92
TR_r=7
##
Motenc1=MOTENC(Enc1A,Enc1B,Motor1A,Motor1B,Motor1E,CPR,GR,TR_r)
Motenc1.start()
#
Tnow=0
Tant=0
interv=3
try:
	while True:
		Tnow=time.perf_counter()
		if Tnow>=Tant+interv:
			Tant=Tnow
		
##			print('Dcycle->   '+str(int(Motenc1.MOT.SP))+'  PID->  '+str(Motenc1.pid)+'   P->  '+str(Motenc1.ENC.P)+'  ERROR->   '+str(Motenc1.ERROR))
			print(Motenc1.ENC.P)
except KeyboardInterrupt:
	GPIO.cleanup()
	print('sacabao')
	Motenc1.join()
	pass
##
##

