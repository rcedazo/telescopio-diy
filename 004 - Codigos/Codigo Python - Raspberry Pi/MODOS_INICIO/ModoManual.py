#!/usr/bin/python3


#Importamos las librerias necesarias

import sys
import time
import socket
import threading
import RPi.GPIO as GPIO
import re
import json
import time
import os
import pandas as pd

from datetime import datetime
from RPLCD import CharLCD
from urllib.request import urlopen
from pandas import DataFrame, read_csv

from FuncionesGenerales import LCD_ON, write_to_lcd, loop_string, Button_State, Lectura_Boton, millis, write_LCD
from FuncionesGenerales import Teclado

#Función para ingresas la ciudad de forma manual

def Input_Ciudad(lcd):
    
    letter = ['A','B','C','D','E','F','G','H','I','J','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']


    print("Datos no recogidos a través de la red, procedeemos a realizar la configuración manual")


    write_LCD(lcd,0,2,'MODO MANUAL',1)
    millis(0)
    write_LCD(lcd,1,4,'ACTIVADO',0)    

    millis(0)

    write_LCD(lcd,0,1,'INSTRUCCIONES',1)

    millis(0)
        
    #Llamada a la función encargada de la escritura en pantalla      

    framebuffer = [
            '',
            '',
            ]
    
    write_to_lcd(lcd, framebuffer, 16)

    #Frase inicial de bienvenida
    long_string = 'Para ingresar la ciudad tendra que moverse con los botones de direcion hasta llegar a la letra requerida, una vez en ella selecionar OK. Si se presionia dos veces consecutivas, se definira la palabra escrita'

    #Función que realiza el scrolling

    #loop_string(long_string, lcd, framebuffer, 1, 16)

    write_LCD(lcd,0,3,'Ingrese la',1)
    write_LCD(lcd,1,5,'ciudad',0)

    millis(0)           

    #Llamada a la función que nos ingresa un teclado en pantalla       
    
    Teclado(lcd,letter)
        

    #Coordenadas de posición dentro de la pantalla
    #FALTARIA POR COMPLETAR CON LOS MOVIMIENTOS LATERALES
    x=0
    y=0
    cont = 0  #Cuenta el numero de letra de las que se compone la ciudad
    exit_modo = 0
    input_ciudad = 'a'

    
    while True:
        
        #El Boton de cancel va a funcionar como  izquierda pero luego deberá ser cambiado
        
        input_state_1 = Lectura_Boton()[0]                                  
        input_state_2 = Lectura_Boton()[1]
        input_state_3 = Lectura_Boton()[2]
        input_state_4 = Lectura_Boton()[3]
        input_state_5 = Lectura_Boton()[4]
        
        if input_state_3 == False:
            
            if x < 9 and y == 1:
                input_ciudad +=  letter[x+y+15]
                print(input_ciudad)
                
                millis(3)
            
            elif y == 0:
                input_ciudad +=  letter[x+y]
                print(input_ciudad)
                
                millis(3)
            
                
            elif x + y == 16:
                
                lcd.cursor_mode = 'hide'
                write_LCD(lcd,0,0,'Ciudad Registrada',1)
                
                #Reiniciamos las variables de salida del bucle
                
                exit_modo = 0
                
                #Eliminamos la pirmera letra de la ciudad introducida por nosotros
                                       
                input_ciudad = input_ciudad[1:]

                write_LCD(lcd,1,0,str(input_ciudad),0)                                                             
                
                millis(0)
                
                while True:
                    
                    if exit_modo  ==  1 or exit_modo == 2:
                        break
                    
                    else:
                                            
                        write_LCD(lcd,0,1,'Teclee ENTER\n\r si es correcto',1)
                        
                        millis(0)
                        
                        write_LCD(lcd,0,0,'Teclee CANCEL si\n\r no es correcto',1)                    
                        
                        millis(0)
                                    
                        while True:
                                    
                            input_state_3 =  Lectura_Boton()[2]  
                            input_state_4 = Lectura_Boton()[3]  
                                    
                            if input_state_3 == False:
                                
                                write_LCD(lcd,0,0,'CONFIRMADO',1)  

                                millis(0)                                    
                                    
                                exit_modo = 1
                                break
         
                                        
                            elif input_state_4 == False:

                                write_LCD(lcd,0,0,'CANCELADO',1)
                                
                                exit_modo =  2
                                input_ciudad = 'a'
                                x=0
                                y=0
                                
                                millis(0)                                        

                                Teclado(lcd,letter)
                                
                                break                            
            else:
                print("ERROR")
                break

                            
        elif input_state_5 == False:                    
            if 15 > x >= 0:
                x = x + 1
                #Nos posicionamos en la letra elegida
                lcd.cursor_pos=(y,x)

                millis(3)

        elif input_state_4 == False:                    
            if 16 >= x > 0:
                x = x - 1
                print(x)
                #Nos posicionamos en la letra elegida
                lcd.cursor_pos=(y,x)

                millis(3)
                    
        elif input_state_2 == False:                    
            if 1 >= y > 0:
                y = y - 1

                #Nos posicionamos en la letra elegida
                lcd.cursor_pos=(y,x)

                millis(3)

        elif input_state_1 == False:                    
            if 1 > y >= 0:
                y = y + 1

                #Nos posicionamos en la letra elegida
                lcd.cursor_pos=(y,x)

                millis(3)
        
        elif exit_modo == 1:
            break
        
    return input_ciudad

#Función que nos va a permitir la búsqueda la latitud y longitud del sisito
def Busqueda_LatLong(lcd,city):
    
    #Una vez obtenida la ciudad de forma manual vamos a proceeder a buscar sus coordenadas en la base de dato  que tenemos
        
    file = r'listado-longitud-latitud.xls'
    df = pd.read_excel(file)

    #Comparamos la variable metida por el usuario con nuestra base de datos y obtenemos la longitud y latitud
    ciudad = 'Madrid'
    long  = len(df['Poblacion'])

    print(city)
            
    write_LCD(lcd,0,3,'OBTENIENDO',1)
    write_LCD(lcd,1,0,'LATITUD/LONGITUD',0)

    i = 0

    for i in range(0,long):
        if ciudad == df['Poblacion'][i]:
            lat = df['Latitud'][i]
            long = df['Longitud'][i]
            alt = df['Altitud'][i]
            
            print(lat, long)
                                                                    
    millis(0)
    
    write_LCD(lcd,0,1,'Latitud',1)
    write_LCD(lcd,1,1, str(lat),0)

    millis(0)
    
    write_LCD(lcd,0,1,'Longitud',1)
    write_LCD(lcd,1,1, str(long),0)

    millis(0)
    
    
    #Registramos la ciudad en nuestro registro
    
    
    return (lat, long)


def Input_Hora(lcd):
    
   
#Una vez ingresada la ciudad, procedemos a la introduccion de la hora de forma manual
    
    write_LCD(lcd,0,2,'Ingresar hora',1)
    write_LCD(lcd,1,5,'HH:MM',0)
 

    millis(0)
        
      
    number = ['0','1','2','3','4','5','6','7','8','9']
    
    #Reiniciliazamos losc ontadores d eposicion
    x=0
    y=0
    i=0
    exit_modo = 0
    
    cont = 0     #Contador  que nos va acontar el numero de numeros
    hora = 'h'
    min = 'm'
#Creacion del script que nos permita leer lo que el usuario nos  meta por pantalla
                
    Teclado(lcd, number)
    
    while True:
                        
        input_state_1 = Lectura_Boton()[0]                                    
        input_state_2 = Lectura_Boton()[1]  
        input_state_3 = Lectura_Boton()[2]
        input_state_4 = Lectura_Boton()[3] 
        input_state_5 = Lectura_Boton()[4] 

        if exit_modo  ==  1:
            break
                
        elif  input_state_3 == False:

            if cont <= 1:
                exit_modo = 0
                hora += number[x]
                cont = cont + 1
                print(hora[1:])
                                
                millis(3)
                    
                
            elif cont <= 3:
                min  += number[x]
                cont = cont +1
                print(min[1:])
                
                millis(3)
            
        elif cont == 4:
            
            hora = hora[1:]
            min  = min[1:]
            
            #Vamos aconsultar  si la hora ingresada es la correcta


            lcd.cursor_mode = 'hide'
            write_LCD(lcd,0,2,'Hora fijada',1)
            write_LCD(lcd,1,5,str(hora),0)            
            write_LCD(lcd,1,7,':',0)  
            write_LCD(lcd,1,8,str(min),0)  

            millis(0)
            
            #Confirmar si la hora introducida es la correcta si no vuelve aincializar el proceso hasta que sea
                    
            while True:
                if exit_modo  ==  1 or exit_modo == 2:
                    break
                            
                else:
                    write_LCD(lcd,0,1,'Teclee ENTER\n\r si es correcto',1)                                   
                                    
                    millis(0)

                    write_LCD(lcd,0,0,'Teclee CANCEL si\n\r no es correcto',1)

                                            
                    while True:
                                            
                        input_state_3 = Lectura_Boton()[2] 
                        input_state_4 = Lectura_Boton()[3] 
                                            
                        if input_state_3 == False:
                            
                            write_LCD(lcd,0,1,'CONFIRMADO',1) 
  
                            millis(0)
                            
                            #Cambiamos la hora de la raspberry manualmente
                        
                            HMS=str(hora) + ':'+ str(min) + ':'+ str(00) 
                            
                            cmd='sudo date +%T -s ' +"'" +str(HMS) +"'"
                            os.system(cmd)
                                                
                            exit_modo = 1
                            break
                 
                                                
                        elif input_state_4 == False:
                            
                            write_LCD(lcd,0,1,'CANCELADO',1) 

                            exit_modo =  2

                            #Reinicializamos todo          
                            cont = 0
                            x=0
                            y=0
                            hora = 'h'
                            min = 'm'                                   

                            millis(0)
                                            
                            Teclado(lcd, number)

                            break
            

            
        elif input_state_5 == False:                    
            if 9 > x >= 0:
                x = x + 1
                
                #Nos posicionamos en la letra elegida
                lcd.cursor_pos=(y,x)

                millis(3)

        elif input_state_4 == False:                    
            if 9 >= x > 0:
                x = x - 1
                #Nos posicionamos en la letra elegida
                lcd.cursor_pos=(y,x)

                millis(3)
          

def Input_Fecha(lcd):
     

    #Por ultimo tendremos que intorducir la fecha en la que estamos

                            
    write_LCD(lcd,0,1,'Ingresar Fecha',1) 
    write_LCD(lcd,1,3,'DD:MM:AAAA',0) 

    millis(0)
        
    
    #Reiniciliazamos losc ontadores d eposicion
    x=0
    y=0
    i=0
    exit_modo = 0
    
    cont = 0     #Contador  que nos va acontar el numero de numeros
    dia = 'd'
    mes = 'm'
    anno = 'a'


    number = ['0','1','2','3','4','5','6','7','8','9']
    
#Sacamos el teclado numerico de nuevo

    Teclado(lcd, number)
        
    while True:
        
                
        input_state_1 = Lectura_Boton()[0]                                   
        input_state_2 = Lectura_Boton()[1] 
        input_state_3 = Lectura_Boton()[2]
        input_state_4 = Lectura_Boton()[3]
        input_state_5 = Lectura_Boton()[4]

        if exit_modo  ==  1:
            break
                
        elif  input_state_3 == False:

            if cont <= 1:
                exit_modo = 0
                dia += number[x]
                cont = cont + 1
                
                print(dia)
                millis(0)
                    
                
            elif 1 < cont <= 3:
                mes += number[x]
                cont = cont +1

                print(mes)
                millis(0)

            elif 3 < cont <= 7:
                anno += number[x]
                cont = cont +1


                print(anno)
                millis(0)
              
                    
        elif cont == 8:
            
            dia =  dia[1:]
            mes  = mes[1:]
            anno = anno[1:]
            
            print(dia)
            print(mes)
            print(anno)
            
            #Vamos aconsultar  si la hora ingresada es la correcta
            lcd.cursor_mode = 'hide'
            
            write_LCD(lcd,0,2,'Fecha Fijada',1)
            write_LCD(lcd,1,2,str(dia),0)
            write_LCD(lcd,1,4,'/',0)
            write_LCD(lcd,1,5,str(mes),0)        
            write_LCD(lcd,1,7,'/',0)
            write_LCD(lcd,1,8,str(anno),0)   
            
            #Confirmar si la hora introducida es la correcta si no vuelve aincializar el proceso hasta que sea
 
            millis(0)
                                                                            
            while True:                                                
                            
                if exit_modo  ==  1 or exit_modo == 2:
                    break
                            
                else:
                    write_LCD(lcd,0,1,'Teclee ENTER\n\r si es correcto',1) 
                                    
                    millis(0)

                    write_LCD(lcd,0,0,'Teclee CANCEL si\n\r no es correcto',1)                                             

                                            
                    while True:
                                            
                        input_state_3 = Lectura_Boton()[2] 
                        input_state_4 = Lectura_Boton()[3]
                                            
                        if input_state_3 == False:
                            
                            write_LCD(lcd,0,1,'CONFIRMADO',1)

                            #Cambiamos la fecha
                        
                            DMY=str(anno) + '-'+ str(mes) + '-'+ str(dia) 
                            
                            cmd='sudo date +%Y-%m-%d -s ' +"'" +str(DMY) +"'"
                            os.system(cmd)
                            millis(2)
                                            
                            exit_modo = 1
                            break
                 
                                               
                        elif input_state_4 == False:
                            
                                                        
                            write_LCD(lcd,0,1,'CANCELADO',1)

                            exit_modo =  2

                            #Reinicializamos todo          
                            cont = 0
                            x=0
                            y=0
                            
                            
                            dia = 'd'
                            mes = 'm'
                            anno = 'a'
    
                            millis(0)                                     
                                            
                            Teclado(lcd, number)

                            break
            

            
        elif input_state_5 == False:                    
            if 9 > x >= 0:
                x = x + 1
                
                #Nos posicionamos en la letra elegida
                lcd.cursor_pos=(y,x)

                millis(3)

        elif input_state_4 == False:                    
            if 9 >= x > 0:
                x = x - 1
                #Nos posicionamos en la letra elegida
                lcd.cursor_pos=(y,x)

                millis(3)
 