#!/usr/bin/python3


#Importamos las librerias necesarias

import sys
import time
import socket
import threading
import RPi.GPIO as GPIO
import re
import json
import time
import os
import pandas as pd
import ControlMando


from pandas import DataFrame, read_csv
from FuncionesGenerales import LCD_ON, write_to_lcd, loop_string, Button_State, Lectura_Boton, millis, write_LCD


def Selec_Nivela(lcd):

    menu_1 = ['METODO DE LA\n\r POLARIS', 'METODO DE LAS\n\r TRES ESTRELLAS']
    menu_2 = ['MET. POLARIS\n\r SELECCIONADO','TRES ESTRELLAS\n\r SELECCIONADO']
    cont_1 = 0
    modo_selec = 0
    
    write_LCD(lcd,0,3,'SELECCIONE',1)  
    write_LCD(lcd,1,1,'MET. NIVELACION',0)      
    

    while True:
        
        
        input_state_1 = Lectura_Boton()[0]

            
        if input_state_1 == False:    
                
            write_LCD(lcd,0,0,str(menu_1[cont_1]),1)                    
                
            millis(0)
            
            while True:
                    input_state_1 = Lectura_Boton()[0]
                    input_state_3 = Lectura_Boton()[2]
                        
                    if input_state_1 == False:
                            
                        if cont_1 >= 1:
                            break
                                               
                        else:
                            cont_1 = cont_1 + 1
                            print(cont_1)

                            write_LCD(lcd,0,0,str(menu_1[cont_1]),1)
                                
                            millis(0)

                            while True:
                                input_state_1 = Lectura_Boton()[0]
                                input_state_2 = Lectura_Boton()[1]
                                input_state_3 = Lectura_Boton()[2]
                                        
                                if input_state_2 == False:
                                            
                                    if cont_1 == 0:
                                        
                                        millis(0)
                                                
                                        break
                                            
                                    else:
                                        cont_1  = cont_1 - 1
                                                
                                        write_LCD(lcd,0,1,str(menu_1[cont_1]),1)                                                            
                                        
                                elif input_state_1 == False:
                                    cont_1 = 0
                                    break
                                    
                                elif input_state_3 == False:
                                        
                                    write_LCD(lcd,0,1,str(menu_2[1]),1)                                      
                                    modo_selec = 2
                                    print(modo_selec)
                                    
                                    millis(0)

                                    break
                            
                    elif modo_selec != 0:
                        break
                            
                    elif input_state_3 == False:
                                                
                        write_LCD(lcd,0,3,str(menu_2[0]),1)  
                        modo_selec = 1
                                
                        millis(0)                   

                        break

        elif modo_selec != 0:
            break
                    
        elif modo_selec == 1 or modo_selec == 2:
            break

    return modo_selec


def Niv_Polaris(lcd):
    
    write_LCD(lcd,0,3,'NIVELE EL',1)  
    write_LCD(lcd,1,1,'TELESCOPIO',0)  

    millis(0)
    
    write_LCD(lcd,0,1,'APUNTE A LA',1)
    write_LCD(lcd,1,0,'ESTRELLA POLARIS',0)

    millis(0)
    
    write_LCD(lcd,0,1,'UNA VEZ LLEGUE',1)
    write_LCD(lcd,1,4,'PULSE V',0)


    #Llamada a la clase que regula la lectura de los botones para que se muevan en una dirección sin PWM
    modo_nivela = ControlMando.control_mando()    
    modo_nivela.bot_nivelacion()
    
    #Nos permite sacar las variables de dentro de la clase
    modo_nivela.modo_funcionamiento   

    
    while True:
       
        input_state_3 = Lectura_Boton()[2]
        
        if input_state_3 == False:
            write_LCD(lcd,0,4,'OBJETO',1)
            write_LCD(lcd,1,3,'CENTRADO',0)            
                          
            file = r'StarDatabase.xls'
            df = pd.read_excel(file)            

            long  = len(df['Name'])

            estrella = 'Polaris'
            
            i = 0

            for i in range(0,long):
                if estrella == df['Name'][i]:
                    RA = df['RA'][i]
                    DEC= df['Dec'][i]
            break
            

    return(RA, DEC)


def Niv_TresEstrellas(lcd,j):
    
    print("DIEEEE")
    print(j)
    
    if j==0:

        #La primera estrella que cojamos la tomara como origen y el movimiento será en manual para llegar hasta ella
        millis(0)
        
        write_LCD(lcd,0,1,'UNA VEZ LLEGUE',1)
        write_LCD(lcd,1,5,'PULSE V',0)
                
        #Llamada a la clase que regula la lectura de los botones para que se muevan en una dirección sin PWM
        modo_nivela = ControlMando.control_mando()    
        modo_nivela.bot_nivelacion()       
    

        while True:
           
            input_state_3 = Lectura_Boton()[2]
            
            if input_state_3 == False:
                write_LCD(lcd,0,4,'OBJETO',1)
                write_LCD(lcd,1,3,'CENTRADO',0)
                break
            
            
    elif j==1:
        #La segunda estrella tendrá un movimiento en automático y se corregirá luego con el modo manual de movimiento
        millis(0)
        
        write_LCD(lcd,0,3,'MOVIENDO',1)
        write_LCD(lcd,1,1,'AUTOMATICAMENTE',0)
            
        millis(0)       
        
        write_LCD(lcd,0,3,'AJUSTAR',1)
        write_LCD(lcd,1,1,'MANUALMENTE',0)
        
        #Llamada a la clase que regula la lectura de los botones para que se muevan en una dirección sin PWM
        modo_nivela = ControlMando.control_mando()    
        modo_nivela.bot_nivelacion()       
    

        while True:
           
            input_state_3 = Lectura_Boton()[2]
            
            if input_state_3 == False:
                write_LCD(lcd,0,4,'OBJETO',1)
                write_LCD(lcd,1,3,'CENTRADO',0)
                break        
            
            
    elif j==2:
        #La tercera estrella tendrá un movimiento completamente en automático ya que el movimiento estaría corregido, si no se llegara al valor correcto se debería rehacer el proceso
        millis(0)
        
        write_LCD(lcd,0,3,'MOVIENDO',1)
        write_LCD(lcd,1,1,'AUTOMATICAMENTE',0)
            
        millis(0)         

        while True:
           
            input_state_3 = Lectura_Boton()[2]
            
            if input_state_3 == False:
                write_LCD(lcd,0,4,'OBJETO',1)
                write_LCD(lcd,1,3,'CENTRADO',0)
                break
            
            
#Función que nos permite  leer las estrellas elegidas por el usuario
def Selector_Estrellas(lcd):

    exit_modo = 0
    i = -1
    
    file = r'StarDatabase.xls'
    df = pd.read_excel(file)            

    long  = len(df['Name'])
    
    
    while True:
        
        
        input_state_1 = Lectura_Boton()[0]
        input_state_2 = Lectura_Boton()[1]
        input_state_3 = Lectura_Boton()[2]
            
        if input_state_1 == False:
            
            if i == long:
                continue
            
            else:
                i = i + 1
                               
                write_LCD(lcd,0,0,str(df['Name'][i]),1)
                write_LCD(lcd,1,0,str(df['RA'][i]),0)
                write_LCD(lcd,1,9,str(df['Dec'][i]),0)
                

                
                print(i)
                millis(3)
                                                                           
        
        elif input_state_2 == False:
                    
            if i == 0:
                
                millis(3)
                        
                continue
                    
            else:
                i = i - 1
                
                write_LCD(lcd,0,0,str(df['Name'][i]),1)                                                            
                write_LCD(lcd,1,0,str(df['RA'][i]),0)
                write_LCD(lcd,1,9,str(df['Dec'][i]),0)
                
                print(i)
                millis(3)
                

        elif input_state_3 == False:
                                    
            write_LCD(lcd,0,1,'CONFIRMADO',1)  

            N_Estrella = str(df['Name'][i])
            Coord_RA = str(df['RA'][i])
            Coord_Dec= str(df['Dec'][i])
                        
            millis(0)                   

            break

                           

    return(N_Estrella, Coord_RA, Coord_Dec)

