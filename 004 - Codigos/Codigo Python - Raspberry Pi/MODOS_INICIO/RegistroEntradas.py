#!/usr/bin/python3


#Importamos las librerias necesarias

import sys
import time
import socket
import threading
import RPi.GPIO as GPIO
import re
import json
import time
import os
import pandas as pd
import MenuModo

from FuncionesGenerales import LCD_ON, write_to_lcd, loop_string, Button_State, Lectura_Boton, millis, write_LCD
from pandas import ExcelWriter
from pandas import ExcelFile
from openpyxl import load_workbook

def Comprobacion_Registro(lcd):
    
    #Inicializamos un contador
    
    i=-1
    
    #Comprobamos si el archivo está vacío o tiene alguna entrada

    write_LCD(lcd,0,1,'COMPROBANDO',1) 
    write_LCD(lcd,1,1,'REGISTROS',0)

    millis(0)


    #Se detecta  que el fichero tiene entradas y se procede a preguntar si quiere cargar alguna

    #Leemos el registro para ver cuanto está escrito
    
    fn2 = r'Registro.xlsx'

    data= pd.read_excel(fn2)
    
    long = len(data['CIUDAD'])
    print(long)

    if long > 0:

        write_LCD(lcd,0,3,'ENTRADAS',1) 
        write_LCD(lcd,1,1,'ENCONTRADAS',0)

        millis(0)

        write_LCD(lcd,0,0,'CARGAR ENTRADA?',1) 
        write_LCD(lcd,1,2,'TECLEE V O X',0)
        
        millis(0)
       
        while True:
            
            input_state_3 = Lectura_Boton()[2]
            input_state_4 = Lectura_Boton()[3]
            
            if input_state_3 == False:
                
                write_LCD(lcd,0,1,'ACCEDIENDO AL',1) 
                write_LCD(lcd,1,3,'REGISTRO',0)
                
                ent_regis=1
                
                break
                
            elif input_state_4 == False:
                
                write_LCD(lcd,0,1,'REGISTRO NUEVA',1) 
                write_LCD(lcd,1,3,'LOCALIZAION',0)
                
                ent_regis=0
                exit_triang = 0
                Reg_Ciudad = 0
                Reg_Lat = 0
                Reg_Long = 0


                break
                

        if ent_regis == 1:

            write_LCD(lcd,0,1,'SELECCIONAR',1) 
            write_LCD(lcd,1,3,'ENTRADA',0)
            
            millis(0)
            

            while True:
                
                
                input_state_1 = Lectura_Boton()[0]
                input_state_2 = Lectura_Boton()[1]
                input_state_3 = Lectura_Boton()[2]
                    
                if input_state_1 == False:
                    
                    if i == long-1:
                        continue
                    
                    else:
                        i = i + 1
                                       
                        write_LCD(lcd,0,0,str(data['CIUDAD'][i]),1)
                                          
                        print(i)
                        millis(3)                                                                              
                
                elif input_state_2 == False:
                            
                    if i == 0:
                        
                        millis(3)
                                
                        continue
                            
                    else:
                        i = i - 1
                        
                        write_LCD(lcd,0,0,str(data['CIUDAD'][i]),1)                                                            
                        
                        print(i)
                        millis(3)
                        

                elif input_state_3 == False:
                                            
                    write_LCD(lcd,0,1,'CONFIRMADO',1)  

                    Reg_Ciudad = str(data['CIUDAD'][i])
                    Reg_Lat = str(data['LATITUD'][i])
                    Reg_Long = str(data['LONGITUD'][i])
                                
                    millis(0)                   
                    exit_triang = 1
                    
                    break
                
            return exit_triang, Reg_Ciudad, Reg_Lat, Reg_Long, ent_regis

                #Se deteca que el fichero está vacío y por tanto al usuario no se le permite acceder a nada
                
    else:
        
        write_LCD(lcd,0,3,'REGISTRO',1) 
        write_LCD(lcd,1,4,'VACIO',0)
        
        millis(0)       
            
        print("Fichero vacio")
        
        exit_triang = 0
        ent_regis = 0
        Reg_Ciudad = 0
        Reg_Lat = 0
        Reg_Long = 0
        
        return exit_triang, Reg_Ciudad, Reg_Lat, Reg_Long, ent_regis

    
    return exit_triang, Reg_Ciudad, Reg_Lat, Reg_Long, ent_regis

def InEntrada(city, LAT, LONG):
       
    #Leemos el registro para ver cuanto está escrito
    
    fn2 = r'Registro.xlsx'

    data= pd.read_excel(fn2)
    
    long = len(data['CIUDAD'])
    print(long)
    
    
    #Metemos en el registro la ciudad  latitud y longitud del sitio deseado

    fn = r'Registro.xlsx'
    
    df= pd.read_excel(fn, header=None)
    df2 = pd.DataFrame({'Ciudad': [city], 'Latitud': [LAT], 'Longitud': [LONG]})
    
    writer = pd.ExcelWriter(fn, engine='openpyxl')
    book = load_workbook(fn)
    
    df.to_excel(writer, header=None, index=None)
    df2.to_excel(writer, sheet_name = 'Sheet1', header=None, index=False, startrow=long+1, startcol=0)

    writer.save()
                
