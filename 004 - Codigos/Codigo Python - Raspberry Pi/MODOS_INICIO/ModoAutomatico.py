#!/usr/bin/python3


#Importamos las librerias necesarias

import sys
import time
import socket
import threading
import RPi.GPIO as GPIO
import re
import json
import time
import os
import pandas as pd
import MenuModo

from datetime import datetime
from RPLCD import CharLCD
from urllib.request import urlopen
from pandas import DataFrame, read_csv


from FuncionesGenerales import LCD_ON, write_to_lcd, loop_string, Button_State, Lectura_Boton, millis, write_LCD

#Función encargada de realizar todo el modo automático

def M_Automatico(lcd):
     
    
    write_LCD(lcd,0,1,'MODO AUTOMATICO',1)           
    write_LCD(lcd,1,4,'ACTIVADO',0) 

    millis(0)
                                
    write_LCD(lcd,0,1,'Triangulando\n\r posicion.....',1)
    
    millis(0)
    

    
def DescargaDatos():
    

    try:
        url1 = 'http://ipinfo.io/json'
        url2='http://worldclockapi.com/api/jsonp/cet/now?callback=mycallback'
        urlopen(url1)
        status = "Connected"
                
    except:
        status = "Not connected"
        pass



    if status == "Connected": # Si se esta conectado
                    
        print('You are '+ status)
        
        try:
                            
            response1 = urlopen(url1).read().decode("utf-8")  #Consulta datos de IP
            response2 = urlopen(url2).read().decode("utf-8")[len('mycallback('):-2] #Consulta hora/fecha

                        
            data1 = json.loads(response1) #Lo carga en json
            data2 = json.loads(response2)
            
            IP=data1['ip']
            org=data1['org']
            city = data1['city']
            country=data1['country']
            region=data1['region']
            loc=data1['loc'].split(',')
            
            #forma_fecha='%Y-%m-%dT%H%M%z'
            #CDT=datetime.strptime(data2['currentDateTime'].replace(':',''), forma_fecha)
            
            fecha=data2['currentDateTime'][:16]
            
            CDT=datetime.strptime(fecha, '%Y-%m-%dT%H:%M')
            #ODT=data2['ordinalDate']
            #CFT=data2['currentFileTime']
            
            print ('Your IP detail\n ')
            print ('IP : {4} \nRegion : {1} \nCountry : {2} \nCity : {3} \nLoc : {0}'.format(loc,region,country,city,IP))
            
            if (CDT.strftime('%Y-%m-%dT%H:%M')==time.strftime('%Y-%m-%dT%H:%M')):  #Compara la hora de internet con la de la Raspberry 
                    print('Just in time ->   ' + time.strftime('%d-%m-%Y %H:%M'))
            
            else:
                    HMS=CDT.strftime('%H') + ':'+CDT.strftime('%M') + ':'+ time.strftime('%S') #Si no son iguales la hora de la Raspberry se sincroniza con la de internet
                    cmd='sudo date +%T -s ' +"'" +str(HMS) +"'"
                    os.system(cmd)
                    print('Not in time \nDate changed to: '+ HMS)                 
                
        except:
            print('ERROR  \n\n')
                            
    return (status, city, loc)


def ComprobacionDatos(lcd, city, loc):
    
    #Variable locales
    
    exit_modo = 0

    #Configuración de los botones

    Button_State()
    
    millis(0)

    write_LCD(lcd,0,1,'Localizacion',1) 
    write_LCD(lcd,1,1,str(city),0) 
    
    millis(0)

    write_LCD(lcd,0,1,'Teclee ENTER\n\r si es correcto',1) 

    millis(0)

    write_LCD(lcd,0,0,'Teclee CANCEL si\n\r no es correcto',1) 
    
    millis(0)
    
    while True:
        
        input_state_3 = Lectura_Boton()[2]
        input_state_4 = Lectura_Boton()[3]
        
        if input_state_3 == False:
            
            
            write_LCD(lcd,0,1,'CONFIRMADO',1) 
            
            millis(0)

            write_LCD(lcd,0,1,'Coor. Altitud',1) 
            write_LCD(lcd,1,1,str(loc[0]),0) 
            
            millis(0)

            write_LCD(lcd,0,1,'Coor. Longitud',1) 
            write_LCD(lcd,1,1,str(loc[1]),0)               
            
            millis(0)
            
            write_LCD(lcd,0,1,'Teclee ENTER\n\r si es correcto',1) 

            millis(0)

            write_LCD(lcd,0,0,'Teclee CANCEL si\n\r no es correcto',1) 
            
            millis(0)
            
            while True:
                
                input_state_3 = Lectura_Boton()[2]
                input_state_4 = Lectura_Boton()[3]
                
                if input_state_3 == False:
                    
                    write_LCD(lcd,0,1,'CONFIRMADO',1)
                    
                    millis(0)
                    
                    exit_modo = 1
                    break

                    
                elif input_state_4 == False:
                    
                    write_LCD(lcd,0,1,'CANCELADO',1)                        
                    millis(0)
                    exit_modo =  1
                     
                    break
            
            continue
        
        elif input_state_4 == False:
            write_LCD(lcd,0,1,'CANCELADO',1) 
            exit_modo = 1          
            break
    
        elif exit_modo == 1:
            break
