#!/usr/bin/python3


#Importamos las librerias necesarias

import sys
import time
import socket
import threading
import RPi.GPIO as GPIO
import re
import json
import time
import os

#Importamos codigos generados por nosotros
import MenuModo
import ModoAutomatico
import ModoManual
import Nivelacion_Montura
import RegistroEntradas
import ControlMando

from FuncionesGenerales import LCD_ON, write_to_lcd, loop_string, Button_State, Lectura_Boton, millis, write_LCD


#Definición de variables globales que vamos a usar para pasaraselas al cliente

global T_RA
global T_DEC
global C_RA
global C_DEC


#Clase que va a encargarse de hacer el setup inicial
class set_up_inicial:
    def __init__(self):
        self.lat_loc=0
        self.long_loc=0
        self.RA_Origen = 0
        self.DEC_Origon = 0
        self.modo_funcio = 0

#Generamos la función que va a hacer el setup del telescopio    

    def puesta_en_marcha(self):
        #Agregamos las variables que nos permitan trabajar con las RA y DEC de la estrellas seleccionadas en la nivelación
        
        N_Star = [0,0,0]
        DEC = [0,0,0]
        RA = [0,0,0]
        
        #Ejecutamos funciones generales como la del LCD
        
        lcd = LCD_ON()
                          

        #COMPROBACION REGISTRO DE ENTRADAS
        
        exit_triang, Reg_Ciudad, Reg_Lat, Reg_Long, ent_regis =  RegistroEntradas.Comprobacion_Registro(lcd)
        
        print(Reg_Ciudad, Reg_Lat, Reg_Long, ent_regis)
        
        #METODO DE INGRESO DE LA ZONA, HORA Y FECHA ACTUAL
        
        if exit_triang == 0 or ent_regis == 0:

            #Ejecutamos el código correspondiente al modo seleccionado por el usuario, de esta función sacaremos el modo seleccionado 
            
            modo_selec = MenuModo.Selector_Modo(lcd)
            
            print(modo_selec)
            
            
            #Si el usuario  seleciona el modo automático el sistema se intenterá  conectar a internet para buscar la localización geográfica
            #y la hora donde se encuentra el individuo de tal forma que no  sea necesario  introducir   ni la latitud y altitud del sitio
            #ni la hora. En caso de que no se detecte pasará automáticamente al modo manual.
                    
            if modo_selec == 2:
                
                ModoAutomatico.M_Automatico(lcd)        
                
                (status, city, loc) = ModoAutomatico.DescargaDatos()       
                
                if status == 'Connected':

                    print(status, city, loc)
                    ModoAutomatico.ComprobacionDatos(lcd, city, loc)         
                      
                    self.lat_loc = loc[0]
                    self.long_loc = loc[1]
                    
                    RegistroEntradas.InEntrada(city, lat, long)
                    
                else:
                    pass
              


        #FALTA POR INCLUIR QUE EL USUARIO CONFRME LA HORA EN LA QUE ESTÁ
                                
                #Modo manual
                               
                               
            elif modo_selec == 1 or status == 'Not connected':
                
                city = ModoManual.Input_Ciudad(lcd)
                    
                (lat, long) = ModoManual.Busqueda_LatLong(lcd,city)
            
            
            self.lat_loc = lat
            self.long_loc = long

            #Metemos en el registro la ciudad con su latitud y longitud
            
            RegistroEntradas.InEntrada(city, lat, long)
            
            ModoManual.Input_Hora(lcd)
                
            ModoManual.Input_Fecha(lcd)

            print("FIN")    
            
     
     #METODO DE NIVELACION DEL TELESCOPIO
            
        modo_nivel = Nivelacion_Montura.Selec_Nivela(lcd)
        
        print(modo_nivel)
        
        while True:
            if modo_nivel == 1:
                self.RA_Origen, self.DEC_Origen = Nivelacion_Montura.Niv_Polaris(lcd)
               
                print(self.RA_Origen, self.DEC_Origen)

                millis(0)

                write_LCD(lcd,0,3,'Nivelacion',1)
                write_LCD(lcd,1,2,'correcta V/X',0) 
   
                while True:
                    
                    input_state_3 = Lectura_Boton()[2]
                    input_state_4 = Lectura_Boton()[3]
                    
                    if input_state_3 == False:
                        millis(0)
                        write_LCD(lcd,0,3,'Nivelacion',1)
                        write_LCD(lcd,1,2,'completada',0)
                        exit_modo = 1
                        
                        break
                    
                    elif input_state_4 == False:
                        break
                        
                #Llamada función Jose que adopta las coordenadas de origen
                
                Ppal.
                    
                                     
            elif modo_nivel == 2:
                
                for j in range(0,3):
                    
                    #LLamada a la función que nos saca el valor de la RA y DEC de las tablas que tenemos de estrellas como base de datos
                    write_LCD(lcd,0,3,'NIVELE EL',1)  
                    write_LCD(lcd,1,1,'TELESCOPIO',0)  

                    millis(0)
                    
                    write_LCD(lcd,0,1,'SELECCIONE UNA',1)
                    write_LCD(lcd,1,4,'ESTRELLA',0)
                    
                    
                    #Llamada a la función para registrar las estrellas con la que vamos a nivelar
                    N_Estrella, Coord_RA, Coord_DEC=Nivelacion_Montura.Selector_Estrellas(lcd)
                 
                    #Damos como coordenada de origen el valor de cada estrella para movimiento en automático             
                    self.RA_Origen = Coord_RA
                    self.DEC_Origen = Coord_DEC
                    
                    print(self.RA_Origen, self.DEC_Origen)
                    
                    
                    Nivelacion_Montura.Niv_TresEstrellas(lcd,j)
 
                millis(0)

                write_LCD(lcd,0,3,'Nivelacion',1)
                write_LCD(lcd,1,2,'correcta V/X',0) 
   
                while True:
                    
                    input_state_3 = Lectura_Boton()[2]
                    input_state_4 = Lectura_Boton()[3]
                    
                    if input_state_3 == False:
                        millis(0)
                        write_LCD(lcd,0,3,'Nivelacion',1)
                        write_LCD(lcd,1,2,'completada',0)
                        exit_modo = 1
                        
                        break
                    
                    elif input_state_4 == False:
                        break
                    
            if exit_modo == 1:
                break

#######################################################################################################################           

#Generamos el menú que va a exitir entre el modo manual de movimiento mediante botones y el modo de comunicación con un cliente externo (KSTARS, STELLARIUM)
        
class modos_funcionamiento:
    def __init__(self):
        self.lat_loc=0
        self.long_loc=0
        self.RA_Origen = 0
        self.DEC_Origen = 0
        self.modo_funcio = 0

    def menu_movimiento(self):
        
        #VARIABLES LOCALES

        
        #Ejecutamos funciones generales como la del LCD
        
        lcd = LCD_ON()
        
        
        menu_1 = ['MODO MANUAL', 'MODO KSTARS']
        menu_2 = ['MODO  MANUAL\n\r SELECCIONADO','MODO KSTARS\n\r SELECCIONADO']
        cont_1 = 0
        
        #Variable que  nos permitirá conocer el modo que el usuario ha seleccionado    
        modo_selec = 0         
           
        #Selección de menú
        

        write_LCD(lcd,0,3,'Selector de',1)
        write_LCD(lcd,1,6,'Modo',0)
            
        while True:
            
            
            input_state_1 = Lectura_Boton()[0]

                
            if input_state_1 == False:    
                    
                lcd.clear()
                lcd.cursor_pos = (0,3)
                lcd.write_string(menu_1[cont_1])          
                    
                millis(0)
                
                while True:
                        input_state_1 = Lectura_Boton()[0]
                        input_state_3 = Lectura_Boton()[2]
                            
                        if input_state_1 == False:
                                
                            if cont_1 >= 1:
                                break
                                                   
                            else:
                                cont_1 = cont_1 + 1
                                print(cont_1)

                                write_LCD(lcd,0,0,str(menu_1[cont_1]),1)
                                    
                                millis(0)

                                while True:
                                    input_state_1 = Lectura_Boton()[0]
                                    input_state_2 = Lectura_Boton()[1]
                                    input_state_3 = Lectura_Boton()[2]
                                            
                                    if input_state_2 == False:
                                                
                                        if cont_1 == 0:
                                            
                                            millis(0)
                                                    
                                            break
                                                
                                        else:
                                            cont_1  = cont_1 - 1
                                                    
                                            write_LCD(lcd,0,1,str(menu_1[cont_1]),1)                                                            
                                            
                                    elif input_state_1 == False:
                                        cont_1 = 0
                                        break
                                        
                                    elif input_state_3 == False:
                                            
                                        write_LCD(lcd,0,1,str(menu_2[1]),1)                                      
                                        
                                        #Asignamos a la variable dentor de la clase el valor para saber el modo
                                        self.modo_funcio = 2
                                        
                                        modo_selec = 2
                                        print(modo_selec)
                                        
                                        #Llamada a la función de comunicación con el cliente externo
                                        
                                        KSTARS_COM = thread_server()
                                        KSTARS_COM.start()
                                        
                                        A=KSTARS_COM.RA_Obj
                                        B=KSTARS_COM.DEC_Obj
                                        C=KSTARS_COM.RA_Act
                                        D=KSTARS_COM.RA_Act
                                        
                                        print(A,B,C,D)

                                        
                                        millis(0)

                                        break
                                
                        elif modo_selec != 0:
                            break
                                
                        elif input_state_3 == False:
                                                    
                            write_LCD(lcd,0,3,str(menu_2[0]),1)
                            
                            #Asignamos a la variable dentor de la clase el valor para saber el modo
                            self.modo_funcio = 1                            
                            
                            modo_selec = 1
                            
                            #Nos metemos en el modo manual
                            
                            modo_nivela = ControlMando.control_mando()    
                            modo_nivela.bot_nivelacion()
                                    
                            millis(0)                   

                            break

            elif modo_selec != 0:
                break
                        
            elif modo_selec == 1 or modo_selec == 2:
                break
            
        
        
        

#######################################################################################################################           

#Definimos el hilo que va a permitir la comunicación con los clientes (KSTARS)

class thread_server (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.RA_Obj = 0
        self.DEC_Obj = 0
        self.RA_Act = 0
        self.DEC_Act = 0
            
    def run(self):
        print("Inicialización del hilo de comunicación")
        self.driver_comunicacion()
        print("Saliendo del hilo de comunicación")
    
#Definición de la función que va a crear el socket de comunicación a traves del protocolo del MEADE 
   
    def driver_comunicacion(self):

        cmd='indi-web -v'
        os.system(cmd)

        host = "192.168.1.110"
        port = 8000

        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.bind((host,port))
        mySocket.listen(1)

        print("HOLA")

        while True:

            conn, addr = mySocket.accept()
            print ("connection from: " + str(addr))
            print("DADA")

            while True:
                data = conn.recv(1024).decode()
                pref = data[0:3]
              
                #Si el usuario teclea el CANCEL en cualquier momento sale de ese modo y vuelve a elegirse uno nuevamente
                    
                input_state_4 = Lectura_Boton()[3]
                
                #print(pref)
                print(data)
                
                #Imprimimos por pantalla las coordenadas en las que nos encontramos
                
                write_LCD(lcd,0,0,'RA:',1)
                write_LCD(lcd,0,5,T_RA,0)             
                write_LCD(lcd,1,0,'DEC:',0)           
                write_LCD(lcd,1,5,T_DEC,0)            



                #Damos la posición de la recta ascensión donde nos encontramos

                if str(data) == ":GR#":

                    threadLock.acquire()
                    T_RA='10:00.0#'
                    
                    #Asignamos valor a la variable de la clase
                    self.RA_Act = T_RA
                    
                    threadLock.release()

                    conn.send(str(T_RA).encode())


                #Damos la coordenada de declinación donde nos encontramos


                elif str(data) == ":GD#":

                    threadLock.acquire()
                    T_DEC='20:00.0#'
                    
                    
                    #Asignamos valor a la variable de la clase
                    self.DEC_Act = T_DEC
                    
                    
                    threadLock.release()

                    conn.send(str(T_DEC).encode())


                #Paramos todos los movimientos

                elif str(data) == ":Q#":

                    print("Deteniendo todos los movimientos del telescopio")


                #Sacamos la RA del objeto selecionado en el cielo

                elif str(pref) == ":Sr":

                    C_RA = data[4:11]

                    print(C_RA)
                    print("Coordenada Recta Ascensión Almacenada: " + str(C_RA))
                    
                                        
                    #Asignamos valor a la variable de la clase
                    self.RA_Obj = C_RA

                    Answer = str(1)
                    conn.send(Answer.encode())

                 #Sacamos la DEC del objeto selecionado en el cielo

                elif str(pref) == ":Sd":

                     C_DEC = data[4:10]

                     print(C_DEC)
                     print("Coordenada Declinación Almacenada: " + str(C_DEC))

                     #Asignamos valor a la variable de la clase
                     self.DEC_Obj = C_DEC

                     Answer = str(1)
                     conn.send(Answer.encode())

                elif str(pref) == ":MS":

                     print("Moviendo Telescoio a la coordenada: , " + str(C_RA),str(C_DEC))

                     Answer = str(0)
                     conn.send(Answer.encode())                
                               
                elif input_state_4 == False:
                    exit_modo=1
                    break

                else:
                    print("Comando Desconocido")
     
            if exit_modo==1:
                break
                print("DADADA")
            
#######################################################################################################################           
 

Set_up=set_up_inicial()
Set_up.puesta_en_marcha()

Modos = modos_funcionamiento()
Modos.menu_movimiento()

##KSTARS_COM = thread_server(1,'Comunicacion')
##KSTARS_COM.start()



