# -*- coding: utf-8 -*-
#!/usr/bin/python3

from math import  sin, cos ,tan , asin, acos, atan,pi

from datetime import datetime

class COORD:
    def __init__(self):
        self.RA=[0,0,0]
        self.DEC=[0,0,0]
        self.LAT=[0,0,0]
        self.LONG=[0,0,0]
        self.ALT=[0,0,0]
        self.AZ=[0,0,0]
        self.TIME=()
    
    def EQabstoALTAZ(self,TIME):
        H, DEC=self.EQabstoEQh(TIME,self.LAT,self.LONG,self.RA,self.DEC)
        ALT , AZ=self.EQhtoALTAZ(self.LAT,H , DEC)
        return ALT, AZ
    
    def ALTAZtoEQabs(self,TIME):
        H,DEC=self.ALTAZtoEQh(self.LAT,self.ALT,self.AZ)
        RA,DEC=self.EQhtoEQabs(TIME,self.LAT,self.LONG,H,DEC)
        return RA,DEC
    
    def GmstoGdec(self,Gr,HtoG=0):
    
        Gdec=(Gr[0]+Gr[1]/60+Gr[2]/3600) # Cambio de sexagesimal a decimal
        if HtoG==1: Gdec*=15 #Si HtoG==1 Cambio de Horas a Grados 360/24=15
        #print(str(Gdec) )
        return Gdec
    
    def GdectoGms(self,Gdec,HtoG=0):
        if HtoG==1: Gdec/=15 #Si HtoG==1 Cambio de Grados a Horas  24/360 = 1/15

        Gr=[0,0,0]
        Gr[0]=int(Gdec)
        Gr[1]=int((Gdec-int(Gdec))*60)
        Gr[2]=((Gdec-int(Gdec))*60-int((Gdec-int(Gdec))*60))*60
        #print(str(Gr))
        return Gr

    
    def ALTAZtoEQh(self,LAT,ALT , AZ): #Recibe altura [º,',''] y azimut [º,',''] 
                             #Y devuelve horaria (hms) y Declinación [º,',''] (Horario)
        LATr=self.GmstoGdec(LAT)*pi/180
        ALTr=self.GmstoGdec(ALT)*pi/180
        AZr =self.GmstoGdec(AZ )*pi/180

        LATr=pi-self.GmstoGdec(LAT)*pi/180

        DECr=asin(sin(LATr)*sin(ALTr)-cos(LATr)*cos(AZr)*cos(ALTr))

        if DECr<0: DECr+=2*pi
##        if DECr>2*pi: DECr-=2*pi
##        if (sin(LATr)*cos(AZr)*cos(ALTr)+cos(LATr)*sin(ALTr)) == 0 :   Hr=0
##        else:
        num=sin(AZr)*cos(ALTr)
        denom=sin(LATr)*cos(AZr)*cos(ALTr)+cos(LATr)*sin(ALTr)

##        print('num  '+str(num)+'  denom  '+str(denom))

        if denom==0:
            denom=0.000001
        if num/denom ==0:
            if denom>=0: Hr=0
            if denom<0: Hr=pi
        if num/denom>0:
            if num>0:
                Hr=atan( num /  denom ) +pi
            if num<0:
                Hr=atan( num /  denom ) 
        if num/denom<0:
            if num>0:
                Hr=atan( num /  denom )
            if num<0:
                Hr=pi+atan( num /  denom )
                


##            Hr=acos((cos(ALTr)*sin(LATr)*cos(AZr)+cos(LATr)*sin(ALTr))/cos(DECr))
##            print(Hr)
##            Hr= asin(  ( sin(AZr)*cos(ALTr) )   /(cos(DECr)))
##            print(Hr)


        if Hr<0: Hr+=2*pi
        if Hr>2*pi: Hr-=2*pi

            
        
        DEC=self.GdectoGms(DECr*180/pi)
        H = self.GdectoGms(Hr*180/pi,1)###!!!! Paso a HORAS de Grados (previamente desde radianes)

        return H, DEC

    def EQhtoALTAZ(self,LAT,H , DEC):   #Recibe horaria (hms) y Declinación [º,',''](Horario)
                                    #Y devuelve altura [º,',''] y azimut [º,',''](Horario)
        LATr=self.GmstoGdec(LAT)*pi/180
        Hr  =self.GmstoGdec(H,1)*pi/180 
        DECr=self.GmstoGdec(DEC)*pi/180
        #
        ALTr=asin(cos(LATr)*cos(Hr)*cos(DECr)+sin(LATr)*sin(DECr))

        if ALTr<0: ALTr+=2*pi
        elif ALTr>2*pi: ALTr-=2*pi

        num=sin(Hr)*cos(DECr)
        denom=    (  sin(LATr)*cos(Hr)*cos(DECr) - cos(LATr)*sin(DECr)  )
##        print('num  '+str(num)+'  denom  '+str(denom))

        if denom==0:
            denom=0.000001

        if num/denom ==0:
            if denom>=0: AZr=0
            if denom<0: AZr=pi
        if num/denom>0:
            if num>0:
                AZr=atan( num /  denom )+pi 
            if num<0:
                AZr=atan( num /  denom ) 
        if num/denom<0:
            if num>0:
                AZr=atan( num /  denom )
            if num<0:
                AZr=pi+atan( num /  denom )
    

        if AZr<0: AZr+=2*pi
        elif AZr>2*pi: AZr-=2*pi

        
        ALT=self.GdectoGms(ALTr*180/pi)
        AZ =self.GdectoGms(AZr*180/pi)

        return ALT, AZ

    def SetLST(self,TIME,LAT,LONG):

        Y=TIME.year
        M=TIME.month
        D=TIME.day
        HR=TIME.hour+TIME.minute/60+TIME.second/3600
        
        if M>=3:
            Y-=1
            M+=12
        A=int(Y/100)
        B=2-A+int(A/4)# formula de Jean Meeus en "Astronomical algorithms" 1998
        JD=int(365.25*(Y+4716))+int(30.6001*(M+1))+(D+HR/24)+B-1524.5#http://personales.unican.es/gonzalmi/ssolar/articulos/calculos.html
        if JD<=2299149.5: JD-=B #dia juliano del 4-Octubre-1582

##          T=(JD- 2415019.5)/36525 # T es el número de siglos julianos de 36525 días medios transcurridos a medianoche de
##                        #Greenwich desde el mediodía medio en Greenwich de 31 de diciembre de 1899.
##          Gr0=((6*3600+38*60+45.836)+8640184.542*T+0.0929*T*T)/3600#Tiempo 0hGr HORAS
##        
##          hJ=24*((Gr0/24)-int(Gr0/24)) #HORA DEL DIA (horas)
##           
##          Gr0hms=self.GdectoGms(Gr0,0) #TSGR A 0h en HMS
##        
##          GrST=hJ+(HR)*1.002737909350795 #TSGr ahora== TSGr0h+ HORA(HDEC)
        T=(JD-2451545)/36525
    
        GrST=(280.46061837+13185000.770053742*T+0.000387933*T*T-T*T*T/38710000)/15 #Horario greenwich en Horas
        GrSH=((GrST/24)-int(GrST/24))*24 #HORA SIDERAL Gr
        GrST=GrSH
        #if GrST<0: GrST+=24
        #if GrST>24: GrST-=24
        #print(GrST)
        LONGh=(LONG[0]+LONG[1]/60+LONG[2]/3600)/15  #lONGITUD DE GMS A HDEC
        #print(LONGh)
        LST=GrST+LONGh
        #print(LST)
##        LSH=((LST/24)-int(LST/24))*24
##        LST=LSH
        return LST
    
    def EQhtoEQabs(self,TIME,LAT,LONG,Hh,DEC):
        H=self.GmstoGdec(Hh)
        LST=self.SetLST(TIME,LAT,LONG)
        RAh=LST-H
        if RAh<0: RAh+=24
        RA=self.GdectoGms(RAh,0) #DE HorarioDec a hms
        return RA, DEC
    
    def EQabstoEQh(self,TIME,LAT,LONG,RAh,DEC):
    
        RA=self.GmstoGdec(RAh) #HMS a Hdec
        LST=self.SetLST(TIME,LAT,LONG) 
        #LST=13.27946620821
        Hd=LST-RA
        if Hd<0: Hd+=24
        H=self.GdectoGms(Hd)
    
        return H , DEC
    
class C_00(COORD):
    def __init__(self,
                 LAT=[40,24,59], #pordefecto coordenadas de madrid
                 LONG=[-3,-42,-9],    #y de polaris
                 TIME=datetime.utcnow(),
                 RA=[2,35,54],
                 DEC=[89,16,49]):
        
        COORD.__init__(self)
        self.LAT=LAT
        self.LONG=LONG
        self.TIME=TIME
        self.RA=RA
        self.DEC=DEC
        


        
##        print('RA0'+str(self.__RA) +'    DEC0' +str(self.__DEC))
##        print(self.__TIME)
        
        self.ALT,self.AZ=self.EQabstoALTAZ(self.TIME)

##        print('ALT0'+str(self.__ALT) +'    AZ0' +str(self.__AZ))    

    def RESET_LOC(self,LAT,LONG):
        self.LAT=LAT
        self.LONG=LONG
        
   
    def RESET_ORI(self,RA,DEC):
        self.TIME=datetime.utcnow()
        self.RA=RA
        self.DEC=DEC

        self.ALT,self.AZ=self.EQabstoALTAZ(self.TIME)
##        print('RESETORIGEN ALT:   '+ str(self.__ALT)+'\t AZ   '+str(self.__AZ))

            
	
class C_obj(COORD):
    def __init__(self,C_00):
        COORD.__init__(self)
        self.ALT=C_00.ALT
        self.AZ=C_00.AZ
        self.LAT=C_00.LAT
        self.LONG=C_00.LONG

##        print('COBJ ALT0'+str(self.ALT0) +'    COBJ AZ0' +str(self.AZ0))  
        
    def RESET0(self,C_00):
##        self.RA =C_00.RA
##        self.DEC=C_00.DEC
        self.ALT=C_00.ALT
        self.AZ=C_00.AZ
        self.LAT=C_00.LAT
        self.LONG=C_00.LONG
    def ACT(self,T):
        self.TIME=T
##        print(T)

##        print('OBJ RA'+str(self.RA) +'   OBJ DEC' +str(self.DEC))
##        print('OBJ ALT0'+str(self.ALT0) +'   OBJ AZ0' +str(self.AZ0))
##        print('OBJ LAT'+str(self.LAT) +'   OBJ LONG ' +str(self.LONG))

        
##        self.ALT0=C_00._C_00__ALT
##        self.AZ0=C_00._C_00__AZ
##        print('OBJ ALT'+str(self.ALT) +'    OBJ AZ' +str(self.AZ))
        
        self.ALT,self.AZ=self.EQabstoALTAZ(self.TIME)
        
##        print('OBJ ALT'+str(self.ALT) +'   OBJ AZ' +str(self.AZ))
    


class C_Curr(COORD):
    def __init__(self,C_00):
        COORD.__init__(self)
        self.ALT = C_00.ALT
        self.AZ  = C_00.AZ
        self.LAT = C_00.LAT
        self.LONG= C_00.LONG
        self.GALT= 0
        self.GAZ = 0
        
    def RESET0(self,C_00):
        self.ALT = C_00.ALT
        self.AZ  = C_00.AZ
        self.LAT = C_00.LAT
        self.LONG= C_00.LONG

    def ACT(self,T):
    
        self.TIME = T

##        print('CURR RA'+str(self.RA) +'   CURR DEC' +str(self.DEC))
##        print('CURR ALT'+str(self.ALT) +'   CURR AZ' +str(self.AZ))
##        print('CURR LAT'+str(self.LAT) +'   CURR LONG ' +str(self.LONG))

        
        self.RA,self.DEC=self.ALTAZtoEQabs(T)
        
##        print('RA'+str(self.RA) +'    DEC' +str(self.DEC)) 



## RA HORA en hms
## LAT LONG DEC ALT AZ en gms
## gms=hms*15
## gms=rad*180/pi

##C=COORD()
#Pasar de EQabs a ATLAZ
#Datos propios de la observación

##Hora=[1,45,20]
##Fecha=[2018,3,19]
##Time=datetime(*(Fecha+Hora))
Time=datetime.utcnow()
LAT_M=[40,24,59]#gms
LONG_M=[-3,-42,-9]

#Datos propios del astro observado
AR_pol=[2,54,54.72]#hms
DEC_pol=[89,20,31.1]#gms

AR2= [13,25,11.53]
DEC2=[-11,-9,-41.4]


##
####Pasar de EQabs a EQh
##AR,DEC=AR2,DEC2
##H_M, DEC_M=C.EQabstoEQh(Time,LAT_M,LONG_M,AR,DEC)
##print('COORDENADAS ORIGINALES::  AR: '+ str(AR)+'  DEC:  '+str(DEC))
##print('PASO EQtoALTAZ  H: '+ str(H_M)+'  DEC: '+str(DEC_M))
######Pasar de EQh a ALTAZ
##Alt,Az=C.EQhtoALTAZ(LAT_M,H_M , DEC_M)
##
##print('                ALT: '+ str(Alt)+'  AZ: '+str(Az))
##
##
##print('LST  ::'+ str(C.SetLST(Time,LAT_M,LONG_M)))
##
##
##H_m,DEC_m=C.ALTAZtoEQh(LAT_M,Alt,Az)
##
##print('PASO ALTAZtoEQ H: '+ str(H_m)+'  DEC: '+str(DEC_m))
##
##
##
##AR,DEC=C.EQhtoEQabs(Time,LAT_M,LONG_M,H_m,DEC_m)
##
##print('               AR: '+ str(AR)+'  DEC: '+str(DEC))
##
